#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
from setuptools import setup

setup(name='phasecalculator',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      description='Package for running of FGIP, solvent similarity and VLE calculations with SSIMPLE in an automated fashion.',
      url='https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/ssimpleapps/phasecalculator',
      author='Mark Driver',
      author_email='mdd31@cantab.ac.uk',
      license='AGPLv3',
      packages=setuptools.find_packages(),
      package_data={'':['test/resources/*', 'test/resources/testfiles/*']},
      install_requires=['lxml','testfixtures','numpy','pandas','scipy','matplotlib','svgutils','phasexmlparser','resultsanalysis', 'xmlvalidator', 'phasexmlcreator', 'puresolventinformation', 'solventmapcreator', 'pytest'],
      zip_safe=False)
