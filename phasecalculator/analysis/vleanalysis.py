#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for VLE analysis.

@author: Mark
"""

import logging
import csv
import phasexmlparser.parsers.mixturevaluesparser as mixparser

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def process_vle_data(phase_filename, csv_filename):
    """Process phase XML and write to csv file.

    Parameters
    ----------
    phase_filename : str
        Phase XML filename.
    csv_filename : str
        CSV filename for output information.

    Returns
    -------
    None.

    """
    mixture_dict = read_data_from_mixtures(phase_filename)
    mixture_info_list = convert_mixture_container_dict_to_list(mixture_dict)
    write_data_to_csv(mixture_info_list, csv_filename)


def write_data_to_csv(mixture_info_list, csv_filename):
    """Write data to csv file.

    Parameters
    ----------
    mixture_info_list : list
        List of phase summary information.
    csv_filename : str
        Filename for output csv file.

    Returns
    -------
    None.

    """
    with open(csv_filename, "w") as outfile:
        csvwriter = csv.writer(outfile, delimiter="\t", lineterminator="\n")
        header_row = [
            "solvent_id",
            "temperature",
            "phase type",
            "concentrations[name, value]",
        ]
        csvwriter.writerow(header_row)
        csvwriter.writerows(mixture_info_list)


def convert_mixture_container_dict_to_list(mixture_container_dict):
    """Convert mixture dictionary to list of lists

    Parameters
    ----------
    mixture_container_dict : dict
        Dictionary of mixture information.

    Returns
    -------
    phase_list_container : list
        List of phase entries for output to file.

    """
    phase_list_container = []
    for mix_id, mixture in mixture_container_dict.items():
        for phase_id, phase_dict in mixture.items():
            phase_list_container.append(convert_phase_dict_to_list(phase_dict))
    return phase_list_container


def convert_phase_dict_to_list(phase_dict):
    """Convert phase dictionary to list for output.

    Parameters
    ----------
    phase_dict : dict
        Dictionary conatining information about a phase.

    Returns
    -------
    phase_list : list
        List of information for output to file.

    """
    phase_list = [
        phase_dict["solvent_id"],
        phase_dict["temperature_value"],
        phase_dict["phase_type"],
    ]

    for inchikey, mol_dict in phase_dict["concentrations"].items():
        phase_list += [inchikey, "{:.10f}".format(mol_dict["concentration_value"])]

    return phase_list


def read_data_from_mixtures(vle_filename):
    """Read Mixture XML.

    Parameters
    ----------
    vle_filename : str
        Filename for phase XML file.

    Returns
    -------
    dict
        Summary of phase concentration information.

    """
    return mixparser.parse_mixture_file(vle_filename)
