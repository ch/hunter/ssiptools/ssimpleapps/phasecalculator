# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script with methods for undertaking FGIP analysis.

@author: Mark
"""

import logging
import pathlib
import numpy as np
import solventmapcreator.solvationcalculation.solvationmapgenerator as solvmapgen
import solventmapcreator.solvationcalculation.fgipmaker as fgipmaker

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

EPSILON_I_LIST = np.array([(float(x) / 10.0) - 10.0 for x in range(101)])

EPSILON_J_LIST = np.array([(float(x) / 10.0) for x in range(51)])


def create_solv_map_and_fgips_from_files(
    frac_occ_filename, polynomial_filename_list, directory, **kwargs
):
    """Creates solvent map and FGIPs and output matrix of numerical values.
    
    The default ranges are used for epsilon: 0< epsilon_j < 5 and 
    -10 < epsilon_i < 0 are used and 8th order polynomial for the binding energy.

    Parameters
    ----------
    frac_occ_filename: str
        Filename for fractional Occupancy XML.
    polynomial_filename_list : list of str
        solvation binding energy polynomial filenames.
    directory : str
        directory name for where to place files.
    **kwargs : TYPE
        DESCRIPTION.

    Returns
    -------
    results : list
        List of results of solvation plot information creation.
    latex_blocks : str
        string containing formatted latex figure blocks for all FGIPs.

    """
    pathlib.Path(directory).mkdir(exist_ok=True)
    results, filenames = create_solvation_plots_from_poly_files(
        EPSILON_I_LIST,
        EPSILON_J_LIST,
        frac_occ_filename,
        polynomial_filename_list,
        8,
        directory=directory,
        **kwargs
    )
    latex_blocks = create_fgips_from_file_list(filenames, directory=directory)
    return results, latex_blocks


def create_fgips_from_file_list(filename_list, **kwargs):
    """Create FGIPs from given solvent map files.

    Parameters
    ----------
    filename_list : list of str
        list of solvent map files.
    directory : str, optional
        directory to place FGIPs in. Default is "".

    Returns
    -------
    latex_blocks : str
        Block of latex text for inclusion of FGIPs into a latex document.
        This is requires conversion of the FGIPs to eps files.

    """
    """This creates the FGIPs from the solvent maps.
    """
    latex_blocks = ""
    for filename in filename_list:
        LOGGER.info("input file")
        LOGGER.info(filename)
        latex_blocks += fgipmaker.create_fgip_from_map(filename, **kwargs)
    return latex_blocks


def create_solvation_plots_from_poly_files(
    epsilon_i_list,
    epsilon_j_list,
    frac_occ_filename,
    polynomial_filename_list,
    polynomial_order,
    **kwargs
):
    """Create solvation maps and writes energy matrices to file for given solvents.

    Parameters
    ----------
    epsilon_i_list : list of float
        epsilon values (x direction).
    epsilon_j_list : list of float
        epsilon values (y direction).
    frac_occ_filename: str
        fractional occupancy information filename.
    polynomial_filename_list : list of str
        solvation binding energy polynomial filenames.
    polynomial_order : int
        order of the polynomial fit to use.
    **kwargs : TYPE
        DESCRIPTION.

    Returns
    -------
    plot_outputs : TYPE
        DESCRIPTION.
    filename_list : TYPE
        DESCRIPTION.

    """
    plot_outputs = []
    filename_list = []
    directory = kwargs.get("directory", "")
    frac_occ_inf_dict = solvmapgen.fractionaloccupancycalculator.read_fractional_occupancy_information(
        frac_occ_filename
    )
    for polynomial_filename in polynomial_filename_list:
        LOGGER.info("polynomial_filename:")
        LOGGER.info(polynomial_filename)
        solvent_id = get_solvent_id_from_poly_filename(polynomial_filename)
        LOGGER.info("solvent_id:")
        LOGGER.info(solvent_id)
        try:
            plot_outputs.append(
                create_solvation_plot(
                    epsilon_i_list,
                    epsilon_j_list,
                    frac_occ_inf_dict,
                    solvent_id,
                    polynomial_filename,
                    polynomial_order,
                    **kwargs
                )
            )
            filename_list.append(
                solvmapgen.create_output_filename_stem(solvent_id, directory)
                + "."
                + kwargs.get("fileformat", "svg")
            )
        except Exception as err:
            plot_outputs.append((solvent_id, err.args))
    return plot_outputs, filename_list


def get_solvent_id_from_poly_filename(
    poly_filename, suffix="binding_poly_fit_split.csv"
):
    """Extracts solvent ID from polynomial filename.

    Parameters
    ----------
    poly_filename : str
        solvent polynomial fit filename.
    suffix : str
        filename suffix to be removed.

    Returns
    -------
    str
        solvent ID.

    """
    return poly_filename.split("/")[-1].replace(suffix, "").replace("_", ",")


def create_solvation_plot(
    epsilon_i_list,
    epsilon_j_list,
    frac_occ_inf_dict,
    solvent_id,
    polynomial_filename,
    polynomial_order,
    **kwargs
):
    """Create solvation map plot and write matrix of values to file.

    Parameters
    ----------
    epsilon_i_list : list of float
        epsilon values (x direction).
    epsilon_j_list : list of float
        epsilon values (y direction).
    frac_occ_inf_dict: dict
        
    solvent_id : str
        solvent ID.
    polynomial_filename : str
        solvation binding energy polynomial filename.
    polynomial_order : int
        order of the polynomial fit to use.
    **kwargs : TYPE
        DESCRIPTION.

    Returns
    -------
    Tuple
        results of solvation map plotting and matrix writing.

    """
    return solvmapgen.create_solvation_plot(
        epsilon_i_list,
        epsilon_j_list,
        frac_occ_inf_dict,
        solvent_id,
        polynomial_filename,
        polynomial_order,
        **kwargs
    )
