# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for System and System Collection classes.

@author: Mark
"""

import logging
from lxml import etree
from phasecalculator.classes.phases import Phases
from phasecalculator.classes.runtimeinformation import RuntimeInformation
from phasecalculator.classes.outputinformation import OutputInformation
from phasecalculator.classes.xmlnamespacing import (
    PHASE_CALCULATOR,
    PHASE_CALC_NAMESPACE_DICT,
)


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SystemCollection(object):
    """System Collection class."""

    def __init__(self, system_list):
        """

        Parameters
        ----------
        system_list : list of System
            List of systems.

        Returns
        -------
        None.

        """
        self.system_list = system_list

    def __eq__(self, other):
        """Overload equality comparison operator.

        Parameters
        ----------
        other : object
            object to compare.

        Returns
        -------
        boolean
            is equal.

        """
        if other is None:
            return False
        if isinstance(other, type(self)):
            if len(self.system_list) != len(other.system_list):
                return False
            sys_comps = all(
                [
                    self.system_list[i] == other.system_list[i]
                    for i in range(len(self.system_list))
                ]
            )
            return sys_comps
        else:
            return False

    @classmethod
    def parse_xml(cls, system_collection_xml):
        """Parse XML representation to new instance.

        Parameters
        ----------
        system_collection_xml : lxml.etree.ElementTree
            Element tree representation of system collection XML.

        Returns
        -------
        SystemCollection
            New instance.

        """
        system_xml_list = system_collection_xml.xpath(
            "phasecalc:System", namespaces=PHASE_CALC_NAMESPACE_DICT
        )
        system_list = [System.parse_xml(system_xml) for system_xml in system_xml_list]
        return SystemCollection(system_list)

    def write_to_xml(self):
        """Write SystemCollection to XML.

        Returns
        -------
        sys_coll_element : lxml.etree.Element
            XML representation of SystemCollection.

        """
        sys_coll_element = etree.Element(
            PHASE_CALCULATOR + "SystemCollection", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        for system in self.system_list:
            sys_coll_element.append(system.write_to_xml())
        return sys_coll_element


class System(object):
    """System class"""

    def __init__(self, phases, runtime_information, output_information):
        """Initialise new System.

        Parameters
        ----------
        phases : Phases
            Phases of interest.
        runtime_information : RuntimeInformation
            Runtime parameter information.
        output_information : OutputInformation
            Output option information.

        Returns
        -------
        None.

        """
        self.phases = phases
        self.runtime_information = runtime_information
        self.output_information = output_information

    def __eq__(self, other):
        """Overload equality comparison operator.

        Parameters
        ----------
        other : object
            object to compare.

        Returns
        -------
        boolean
            is equal.

        """
        if other is None:
            return False
        if isinstance(other, type(self)):
            return (
                self.phases == other.phases
                and self.runtime_information == other.runtime_information
                and self.output_information == other.output_information
            )
        else:
            return False

    @classmethod
    def parse_xml(cls, system_xml):
        """Parse XML representation to new instance.

        Parameters
        ----------
        system_xml : lxml.etree.ElementTree
            Element tree representation of system XML.

        Returns
        -------
        System
            New instance.

        """
        phases_xml = system_xml.xpath(
            "phasecalc:Phases", namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        runtime_xml = system_xml.xpath(
            "phasecalc:RuntimeInformation", namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        output_xml = system_xml.xpath(
            "phasecalc:OutputInformation", namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        phases = Phases.parse_xml(phases_xml)
        runtime_information = RuntimeInformation.parse_xml(runtime_xml)
        output_information = OutputInformation.parse_xml(output_xml)
        return System(phases, runtime_information, output_information)

    def write_to_xml(self):
        """Write System to XML.

        Returns
        -------
        system_element : lxml.etree.Element
            XML representation of System.

        """
        system_element = etree.Element(
            PHASE_CALCULATOR + "System", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        system_element.append(self.phases.write_to_xml())
        system_element.append(self.runtime_information.write_to_xml())
        system_element.append(self.output_information.write_to_xml())
        return system_element

    def get_phase_compositions_by_temperature(self):
        """Get phases split up by temperature. This is for solvent XML generation.

        Returns
        -------
        phase_dict : dict 
            Temperature: list of Phase objects pairs.

        """
        return self.phases.get_phase_molefractions_by_temperature()

    def get_phases_by_temp_list(self):
        """Get temperature information and phase composition list for XML creation.

        Returns
        -------
        phase_comps_by_temp : list of lists
            List containing temperature: phase mole fractions pairs for phases to
            create XML for. Phase mole fractions contains a list of dictionaries
            of name: mole fraction pairs per phase to create. Temperature
            information with dictionaries containing value and unit.

        """
        phase_comps_by_temp = self.get_phase_compositions_by_temperature()
        phase_comp_by_temp_list = []
        for temperature, phases_mol_frac_list in phase_comps_by_temp.items():
            phase_comp_by_temp_list.append(
                [[temperature.to_dict()], phases_mol_frac_list]
            )
        return phase_comp_by_temp_list

    def get_molefractions_by_molecule_list(self):
        """List of molefraction dictionaries for all phases in collection.

        Returns
        -------
        mole_fraction_dict_list : list of dict
            list of phase molefraction data.

        """
        return self.phases.get_molefractions_by_molecule_list()

    def get_ssip_file_locations(self):
        """Get the SSIP file locations for all molecules in any phase.

        Returns
        -------
        set of str
            Set of unique SSIP file locations.

        """
        return self.phases.get_ssip_file_locations()

    def get_name_inchikey_map(self):
        """Get the name inchikey mapping for all molecules in the phases.

        Returns
        -------
        name_inchikey_map : dict
            name: inchikey pairings for molecules.

        """
        return self.phases.get_name_inchikey_map()

    def calc_fgip(self):
        """Boolean indicating whether to Calculate FGIP information for Phases.

        Returns
        -------
        bool
            calc FGIP.

        """
        return self.output_information.fgip_output

    def calc_similarity(self):
        """Boolean indicating whether to calculate similarity information for Phases.

        Returns
        -------
        bool
            calc Similarity.

        """
        return self.output_information.similarity_output

    def calc_vle(self):
        """Boolean indicating whether to calculate VLE information for Phases.

        Returns
        -------
        bool
            calc VLE.

        """
        return self.output_information.vle_output
