# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for molecule class.

@author: Mark
"""

import logging
from lxml import etree
from phasecalculator.classes.xmlnamespacing import (
    PHASE_CALCULATOR,
    PHASE_CALC_NAMESPACE_DICT,
)

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class Molecule(object):
    """Molecule class."""

    TOLERANCE = 0.0001

    def __init__(self, name, inchikey, ssip_file_loc, molefraction):
        """Initialise object.

        Parameters
        ----------
        name : str
            molecule name.
        inchikey : str
            InChIKey.
        ssip_file_loc : str
            SSIP file location.
        molefraction : float
            Mole fraction.

        Returns
        -------
        None.

        """
        self.name = name
        self.inchikey = inchikey
        self.ssip_file_loc = ssip_file_loc
        self.molefraction = molefraction

    def __eq__(self, other):
        """Overload equality comparison operator.

        Parameters
        ----------
        other : object
            object to compare.

        Returns
        -------
        boolean
            is equal.

        """
        if other is None:
            return False
        if isinstance(other, type(self)):
            return (
                abs(self.molefraction - other.molefraction) < self.TOLERANCE
                and self.name == other.name
                and self.inchikey == other.inchikey
                and self.ssip_file_loc == other.ssip_file_loc
            )
        else:
            return False

    @classmethod
    def parse_xml(cls, molecule_xml):
        """Parse XML representation to new instance.

        Parameters
        ----------
        molecule_xml : lxml.etree.ElementTree
            Element tree representation of molecule XML.

        Returns
        -------
        Molecule
            New instance.

        """
        name = molecule_xml.xpath(
            "@phasecalc:name", namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        inchikey = molecule_xml.xpath(
            "@phasecalc:inChIKey", namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        ssip_file_loc = molecule_xml.xpath(
            "@phasecalc:ssipFileLocation", namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        molefraction = float(
            molecule_xml.xpath(
                "@phasecalc:molefraction", namespaces=PHASE_CALC_NAMESPACE_DICT
            )[0]
        )
        return Molecule(name, inchikey, ssip_file_loc, molefraction)

    def write_to_xml(self):
        """Write information to Etree representation of XML.

        Returns
        -------
        mol_element : lxml.etree.Element
            XML representation of Molecule.

        """
        mol_element = etree.Element(
            PHASE_CALCULATOR + "Molecule", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        mol_element.set(PHASE_CALCULATOR + "name", self.name)
        mol_element.set(PHASE_CALCULATOR + "inChIKey", self.inchikey)
        mol_element.set(PHASE_CALCULATOR + "ssipFileLocation", self.ssip_file_loc)
        mol_element.set(
            PHASE_CALCULATOR + "molefraction", "{:.4f}".format(self.molefraction)
        )
        return mol_element
