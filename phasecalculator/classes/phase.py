# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Class for representing Phase.

@author: Mark
"""

import logging
from lxml import etree
from phasecalculator.classes.xmlnamespacing import (
    PHASE_CALCULATOR,
    PHASE_CALC_NAMESPACE_DICT,
)
from phasecalculator.classes.molecule import Molecule
from phasecalculator.classes.temperature import Temperature

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class Phase(object):
    """Phase object."""

    def __init__(self, molecule_list, temperature):
        """Initialise Phase.

        Parameters
        ----------
        molecule_list : list of Molecule objects
            Molecules in phase.
        temperature : Temperature
            Temperature of phase.

        Returns
        -------
        None.

        """
        self.molecule_list = molecule_list
        self.temperature = temperature
        if not self.total_molefraction_is_one():
            raise ValueError("Total molefractions of phase not equal to one")

    def __eq__(self, other):
        """Overload equality comparison operator.

        Parameters
        ----------
        other : object
            object to compare.

        Returns
        -------
        boolean
            is equal.

        """
        if other is None:
            return False
        if isinstance(other, type(self)):
            if len(self.molecule_list) != len(other.molecule_list):
                return False
            mol_comps = all(
                [
                    self.molecule_list[i] == other.molecule_list[i]
                    for i in range(len(self.molecule_list))
                ]
            )
            return self.temperature == other.temperature and mol_comps
        else:
            return False

    @classmethod
    def parse_xml(cls, phase_xml):
        """Parse XML representation to new instance.

        Parameters
        ----------
        phase_xml : lxml.etree.ElementTree
            Element tree representation of phase XML.

        Returns
        -------
        Phase
            New instance.

        """

        molecule_list = cls.parse_molecules(phase_xml)
        temp_xml = phase_xml.xpath(
            "phasecalc:Temperature", namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        temperature = cls.parse_temperature(temp_xml)
        return Phase(molecule_list, temperature)

    @classmethod
    def parse_molecules(cls, phase_xml):
        """Parse list of molecule XML.

        Parameters
        ----------
        molecule_xml_list : list of lxml.etree.ElementTree
            Element tree representation of molecule XML.

        Returns
        -------
        list of Molecule
            Molecules in phase.

        """
        molecule_xpath = "phasecalc:Molecule"
        molecule_xml_list = phase_xml.xpath(
            molecule_xpath, namespaces=PHASE_CALC_NAMESPACE_DICT
        )
        return [Molecule.parse_xml(molecule_xml) for molecule_xml in molecule_xml_list]

    @classmethod
    def parse_temperature(cls, temperature_xml):
        """Parse temperature XML.

        Parameters
        ----------
        temperature_xml : lxml.etree.ElementTree
            Element tree representation of temperature XML.

        Returns
        -------
        Temperature
            Temperature object.

        """
        return Temperature.parse_xml(temperature_xml)

    def total_molefraction_is_one(self):
        """Calculate total molefraction of the molecule and check this is 1.0.

        Returns
        -------
        bool
            True if total molefraction of components is one.

        """
        molefraction_total = 0.0
        for mole_frac in self.get_molefractions_by_molecule().values():
            molefraction_total += mole_frac
        return abs(1.0 - molefraction_total) < 1e-5

    def write_to_xml(self):
        """Write information to Etree representation of XML.

        Returns
        -------
        phase_element : lxml.etree.Element
            XML representation of Phase.

        """
        phase_element = etree.Element(
            PHASE_CALCULATOR + "Phase", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        for molecule in self.molecule_list:
            phase_element.append(molecule.write_to_xml())
        phase_element.append(self.temperature.write_to_xml())
        return phase_element

    def get_molefractions_by_molecule(self):
        """Get the molefractions of all molecules in the phase, labelled by name.

        Returns
        -------
        mole_fraction_by_name : dict
            Dictionary of name : molefraction pairs.

        """
        mole_fraction_by_name = {}
        for molecule in self.molecule_list:
            mole_fraction_by_name[molecule.name] = molecule.molefraction
        return mole_fraction_by_name

    def get_ssip_file_locations(self):
        """Get SSIP file locations ofr all molecules in the phase.

        Returns
        -------
        list
            List of SSIP file locations.

        """
        return [molecule.ssip_file_loc for molecule in self.molecule_list]

    def get_name_inchikey_map(self):
        """Get the name inchikey mapping for all molecules in the phases.

        Returns
        -------
        name_inchikey_map : dict
            name: inchikey pairings for molecules.

        """
        name_inchikey_map = {}
        for molecule in self.molecule_list:
            name_inchikey_map[molecule.name] = molecule.inchikey
        return name_inchikey_map
