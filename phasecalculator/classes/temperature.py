# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for temperature class.

@author: Mark
"""

import logging
from lxml import etree
from phasecalculator.classes.xmlnamespacing import (
    PHASE_CALCULATOR,
    PHASE_CALC_NAMESPACE_DICT,
)

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class Temperature(object):
    """Temperature class."""

    TOLERANCE = 0.01

    def __init__(self, temperature, temperature_unit):
        """Initialise object.

        Parameters
        ----------
        temperature : float
            Temperature value.
        temperature_unit : str
            Unit for temperature.

        Returns
        -------
        Temperature

        """
        self.temperature = temperature
        self.temperature_unit = temperature_unit

    def __eq__(self, other):
        """Overload equality comparison operator.

        Parameters
        ----------
        other : object
            object to compare.

        Returns
        -------
        boolean
            is equal.

        """
        if other is None:
            return False
        if isinstance(other, type(self)):
            return (
                abs(self.temperature - other.temperature) < self.TOLERANCE
                and self.temperature_unit == other.temperature_unit
            )
        else:
            return False

    def __hash__(self):
        """Overload hash operator of Temperature class.
        
        Convert Temperature float to kelvin, and write to formatted string to
        2dp. This matches tolerance of numerical values.

        Returns
        -------
        int
            hash of temperature object.

        """
        temperature = (
            self.temperature
            if self.temperature_unit == "KELVIN"
            else self.temperature + 273.15
        )
        temp_str = "{:.2f}".format(abs(temperature))
        return hash(temp_str) + hash(self.temperature_unit)

    @classmethod
    def parse_xml(cls, temperature_xml):
        """Parse XML representation to new instance.

        Parameters
        ----------
        temperature_xml : lxml.etree.ElementTree
            Element tree representation of temperature XML.

        Returns
        -------
        Temperature
            New instance.

        """
        temp_xpath = "@phasecalc:value"
        temperature = float(
            temperature_xml.xpath(temp_xpath, namespaces=PHASE_CALC_NAMESPACE_DICT)[0]
        )
        unit_xpath = "@phasecalc:unit"
        temperature_unit = temperature_xml.xpath(
            unit_xpath, namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        return Temperature(temperature, temperature_unit)

    def write_to_xml(self):
        """Write information to Etree representation of XML.

        Returns
        -------
        temp_element : lxml.etree.Element
            XML representation of Temperature.

        """
        temp_element = etree.Element(
            PHASE_CALCULATOR + "Temperature", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        temp_element.set(PHASE_CALCULATOR + "value", "{:.3f}".format(self.temperature))
        temp_element.set(PHASE_CALCULATOR + "unit", self.temperature_unit)
        return temp_element

    def to_dict(self):
        """Convert to dictionary.

        Returns
        -------
        dict
            Dictionary representation.

        """
        return {
            "temperature_value": self.temperature,
            "temperature_units": self.temperature_unit,
        }
