# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for Phases class.

@author: Mark
"""

import logging
from lxml import etree
from phasecalculator.classes.phase import Phase
from phasecalculator.classes.xmlnamespacing import (
    PHASE_CALCULATOR,
    PHASE_CALC_NAMESPACE_DICT,
)

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class Phases(object):
    """Phases class."""

    def __init__(self, phase_list):
        """Initialise Phases.

        Parameters
        ----------
        phase_list : list of Phase objects.
            Phases for calculation.

        Returns
        -------
        New Phases Instance.

        """
        self.phase_list = phase_list

    def __eq__(self, other):
        """Overload equality comparison operator.

        Parameters
        ----------
        other : object
            object to compare.

        Returns
        -------
        boolean
            is equal.

        """
        if other is None:
            return False
        if isinstance(other, type(self)):
            if len(self.phase_list) != len(other.phase_list):
                return False
            phase_comps = all(
                [
                    self.phase_list[i] == other.phase_list[i]
                    for i in range(len(self.phase_list))
                ]
            )
            return phase_comps
        else:
            return False

    @classmethod
    def parse_xml(cls, phases_xml):
        """Parse XML representation to new instance.

        Parameters
        ----------
        phases_xml : lxml.etree.ElementTree
            Element tree representation of phases XML.

        Returns
        -------
        Phases
            New instance.

        """
        phase_xpath = "phasecalc:Phase"
        phase_xml_list = phases_xml.xpath(
            phase_xpath, namespaces=PHASE_CALC_NAMESPACE_DICT
        )
        phase_list = [Phase.parse_xml(phase_xml) for phase_xml in phase_xml_list]
        return Phases(phase_list)

    def write_to_xml(self):
        """Write phases to XML.

        Returns
        -------
        phases_element : lxml.etree.Element
            XML representation of Phases.

        """
        phases_element = etree.Element(
            PHASE_CALCULATOR + "Phases", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        for phase in self.phase_list:
            phases_element.append(phase.write_to_xml())
        return phases_element

    def get_phase_compositions_by_temperature(self):
        """Get phases split up by temperature. This is for solvent XML generation.

        Returns
        -------
        phase_dict : dict 
            Temperature: list of Phase objects pairs.

        """
        phase_dict = {}
        for phase in self.phase_list:
            if phase.temperature in phase_dict.keys():
                phase_dict[phase.temperature].append(phase)
            else:
                phase_dict[phase.temperature] = [phase]
        return phase_dict

    def get_phase_molefractions_by_temperature(self):
        """Get molefraction dictionaries for all phases in collection grouped by temperature.

        Returns
        -------
        phase_dict : dict
            Temperature: list of phase molefraction data pairs.

        """
        phase_dict = {}
        for phase in self.phase_list:
            if phase.temperature in phase_dict.keys():
                phase_dict[phase.temperature].append(
                    phase.get_molefractions_by_molecule()
                )
            else:
                phase_dict[phase.temperature] = [phase.get_molefractions_by_molecule()]
        return phase_dict

    def get_molefractions_by_molecule_list(self):
        """List of molefraction dictionaries for all phases in collection.

        Returns
        -------
        mole_fraction_dict_list : list of dict
            list of phase molefraction data.

        """
        mole_fraction_dict_list = []
        for phase in self.phase_list:
            mole_fraction_dict_list.append(phase.get_molefractions_by_molecule())
        return mole_fraction_dict_list

    def get_ssip_file_locations(self):
        """Get the SSIP file locations for all molecules in any phase.

        Returns
        -------
        set of str
            Set of unique SSIP file locations.

        """
        ssip_file_locations = []
        for phase in self.phase_list:
            ssip_file_locations.extend(phase.get_ssip_file_locations())
        return set(ssip_file_locations)

    def get_name_inchikey_map(self):
        """Get the name inchikey mapping for all molecules in the phases.

        Returns
        -------
        name_inchikey_map : dict
            name: inchikey pairings for molecules.

        """
        name_inchikey_map = {}
        for phase in self.phase_list:
            name_inchikey_map.update(phase.get_name_inchikey_map())
        return name_inchikey_map
