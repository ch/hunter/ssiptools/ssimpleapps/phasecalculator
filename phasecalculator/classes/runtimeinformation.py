# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Class for runtime information.

@author: Mark
"""

import logging
from lxml import etree
from phasecalculator.classes.xmlnamespacing import (
    PHASE_CALCULATOR,
    PHASE_CALC_NAMESPACE_DICT,
)

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class RuntimeInformation(object):
    """Class representation of runtime information."""

    def __init__(self, phasetransfer_jar, scratch_dir, output_dir):
        """Initialise RuntimeInformation.

        Parameters
        ----------
        phasetransfer_jar : str
            Jar file location.
        scratch_dir : str
            Scratch directory path.
        output_dir : str
            Output directory path.

        Returns
        -------
        RuntimeInformation.

        """
        self.phasetransfer_jar = phasetransfer_jar
        self.scratch_dir = scratch_dir
        self.output_dir = output_dir

    def __eq__(self, other):
        """Overload equality comparison operator.

        Parameters
        ----------
        other : object
            object to compare.

        Returns
        -------
        boolean
            is equal.

        """
        if other is None:
            return False
        if isinstance(other, type(self)):
            return (
                self.phasetransfer_jar == other.phasetransfer_jar
                and self.scratch_dir == other.scratch_dir
                and self.output_dir == other.output_dir
            )
        else:
            return False

    @classmethod
    def parse_xml(cls, runtime_xml):
        """Parse runtime information XML to populate new instance.

        Parameters
        ----------
        runtime_xml : lxml.etree.Element
            XML representation of RuntimeInformation.

        Returns
        -------
        RuntimeInformation
            RuntimeInformation.

        """
        phasetransfer_jar = cls.parse_phasetransfer_jar(runtime_xml)
        scratch_dir = cls.parse_scratch_dir(runtime_xml)
        output_dir = cls.parse_output_dir(runtime_xml)
        return RuntimeInformation(phasetransfer_jar, scratch_dir, output_dir)

    @classmethod
    def parse_phasetransfer_jar(cls, runtime_xml):
        """Parse phasetransfer jar file path.

        Parameters
        ----------
        runtime_xml : lxml.etree.Element
            XML representation of RuntimeInformation.

        Returns
        -------
        phasetransfer_jar : str
            Jar file location.

        """
        xpath_expression = "phasecalc:PhaseTransferJar/text()"
        phasetransfer_jar = runtime_xml.xpath(
            xpath_expression, namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        return phasetransfer_jar

    @classmethod
    def parse_scratch_dir(cls, runtime_xml):
        """Parse scratch directory path.

        Parameters
        ----------
        runtime_xml : lxml.etree.Element
            XML representation of RuntimeInformation.

        Returns
        -------
        scratch_dir : str
            scratch directory.

        """
        xpath_expression = "phasecalc:ScratchDirectory/text()"
        scratch_dir = runtime_xml.xpath(
            xpath_expression, namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        return scratch_dir

    @classmethod
    def parse_output_dir(cls, runtime_xml):
        """Parse output directory from given XML.

        Parameters
        ----------
        cls : TYPE
            DESCRIPTION.
        runtime_xml : lxml.etree.Element
            XML representation of RuntimeInformation.

        Returns
        -------
        output_dir : str
            output directory.

        """
        xpath_expression = "phasecalc:OutputDirectory/text()"
        output_dir = runtime_xml.xpath(
            xpath_expression, namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        return output_dir

    def write_to_xml(self):
        """Write information to Etree representation of XML.

        Returns
        -------
        run_element : lxml.etree.Element
            XML representation of RuntimeInformation.

        """
        run_element = etree.Element(
            PHASE_CALCULATOR + "RuntimeInformation", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        run_element.append(self.write_phasetransfer_jar_xml())
        run_element.append(self.write_scratch_dir_xml())
        run_element.append(self.write_output_dir_xml())
        return run_element

    def write_phasetransfer_jar_xml(self):
        """Write PhaseTransferJar element.

        Returns
        -------
        pt_jar_element : lxml.etree.Element
            XML representation of PhaseTransferJar.

        """
        pt_jar_element = etree.Element(
            PHASE_CALCULATOR + "PhaseTransferJar", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        pt_jar_element.text = self.phasetransfer_jar
        return pt_jar_element

    def write_scratch_dir_xml(self):
        """Write ScratchDirectory element.

        Returns
        -------
        scratch_element : lxml.etree.Element
            XML representation of ScratchDirectory.

        """
        scratch_element = etree.Element(
            PHASE_CALCULATOR + "ScratchDirectory", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        scratch_element.text = self.scratch_dir
        return scratch_element

    def write_output_dir_xml(self):
        """Write OutputDirectory element.

        Returns
        -------
        output_element : lxml.etree.Element
            XML representation of OutputDirectory.

        """
        output_element = etree.Element(
            PHASE_CALCULATOR + "OutputDirectory", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        output_element.text = self.output_dir
        return output_element
