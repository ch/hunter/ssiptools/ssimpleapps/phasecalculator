# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Class for output information.

@author: Mark
"""

import logging
from lxml import etree
from phasecalculator.classes.xmlnamespacing import (
    PHASE_CALCULATOR,
    PHASE_CALC_NAMESPACE_DICT,
)


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class OutputInformation(object):
    """Class representation of output information."""

    def __init__(
        self, fgip_output, similarity_output, similarity_output_type, vle_output
    ):
        """Initialise OutputInformation.

        Parameters
        ----------
        fgip_output : bool
            FGIP output result.
        similarity_output : bool
            Similarity output result.
        similarity_output_type : str
            Output type, see schema for restriction.
        vle_output : bool
            VLE output result.

        Returns
        -------
        OutputInformation
            Initialise new OutputInformation.

        """
        self.fgip_output = fgip_output
        self.similarity_output = similarity_output
        self.similarity_output_type = similarity_output_type
        self.vle_output = vle_output

    def __eq__(self, other):
        """Overload equality comparison operator.

        Parameters
        ----------
        other : object
            object to compare.

        Returns
        -------
        boolean
            is equal.

        """
        if other is None:
            return False
        if isinstance(other, type(self)):
            return (
                self.fgip_output == other.fgip_output
                and self.similarity_output == other.similarity_output
                and self.vle_output == other.vle_output
            )
        else:
            return False

    @classmethod
    def parse_xml(cls, output_xml):
        """Parse XML representation.

        Parameters
        ----------
        output_xml : etree.Element
            OutputInformation element representation.

        Returns
        -------
        OutputInformation
            New class instance.

        """
        fgip_output = OutputInformation.parse_fgip_output(output_xml)
        (
            similarity_output,
            similarity_output_type,
        ) = OutputInformation.parse_similarity_output(output_xml)
        vle_output = OutputInformation.parse_vle_output(output_xml)
        return OutputInformation(
            fgip_output, similarity_output, similarity_output_type, vle_output
        )

    @classmethod
    def parse_fgip_output(cls, output_xml):
        """Parse FGIP output information from from output information element.

        Parameters
        ----------
        output_xml : etree.Element
            OutputInformation element representation.

        Returns
        -------
        bool
            FGIP output result.

        """
        xpath_expression = "phasecalc:FGIPOutput/text()"
        fgip_output = output_xml.xpath(
            xpath_expression, namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        return True if fgip_output == "true" else False

    @classmethod
    def parse_similarity_output(cls, output_xml):
        """Parse similarity output information from output information element.

        Parameters
        ----------
        output_xml : etree.Element
            OutputInformation element representation.

        Returns
        -------
        similarity_output : bool
            Similarity output result.
        similarity_output_type : str
            Output type, see schema for restriction.

        """
        xpath_expression = "phasecalc:SimilarityOutput/text()"
        similarity_output = output_xml.xpath(
            xpath_expression, namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        similarity_output = True if similarity_output == "true" else False
        if similarity_output:
            type_xpath = "phasecalc:SimilarityOutput/@phasecalc:outputType"
            similarity_output_type = output_xml.xpath(
                type_xpath, namespaces=PHASE_CALC_NAMESPACE_DICT
            )[0]
        else:
            similarity_output_type = None
        return similarity_output, similarity_output_type

    @classmethod
    def parse_vle_output(cls, output_xml):
        """Parse VLE from output information element.

        Parameters
        ----------
        output_xml : etree.Element
            OutputInformation element representation.

        Returns
        -------
        bool
            VLE output option.

        """
        xpath_expression = "phasecalc:VLEOutput/text()"
        vle_output = output_xml.xpath(
            xpath_expression, namespaces=PHASE_CALC_NAMESPACE_DICT
        )[0]
        LOGGER.debug("VLE output: %s", vle_output)
        return True if vle_output == "true" else False

    def write_to_xml(self):
        """Write information to Etree representation of XML.

        Returns
        -------
        out_element : lxml.etree.Element
            XML representation of OutputInformation.

        """
        out_element = etree.Element(
            PHASE_CALCULATOR + "OutputInformation", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        out_element.append(self.write_fgip_output())
        out_element.append(self.write_similarity_output())
        out_element.append(self.write_vle_output())
        return out_element

    def write_fgip_output(self):
        """Write FGIP output XML Element.

        Returns
        -------
        fgip_element : etree.Element
            FGIP element representation.

        """
        fgip_element = etree.Element(
            PHASE_CALCULATOR + "FGIPOutput", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        fgip_element.text = str(self.fgip_output).lower()
        return fgip_element

    def write_similarity_output(self):
        """Write Similarity output XML Element.

        Returns
        -------
        sim_element : etree.Element
            Similarity element representation.

        """
        sim_element = etree.Element(
            PHASE_CALCULATOR + "SimilarityOutput", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        sim_element.text = str(self.similarity_output).lower()
        if self.similarity_output_type is not None:
            sim_element.set(
                PHASE_CALCULATOR + "outputType", self.similarity_output_type
            )
        return sim_element

    def write_vle_output(self):
        """Write VLE output XML Element.

        Returns
        -------
        vle_element : etree.Element
            VLE element representation.

        """
        vle_element = etree.Element(
            PHASE_CALCULATOR + "VLEOutput", nsmap=PHASE_CALC_NAMESPACE_DICT
        )
        vle_element.text = str(self.vle_output).lower()
        return vle_element
