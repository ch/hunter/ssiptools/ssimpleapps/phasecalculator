# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for reading CSV file to Phases object.

@author: Mark
"""

import logging
import csv
import puresolventinformation.information as pureinf
from phasecalculator.classes.temperature import Temperature
from phasecalculator.classes.molecule import Molecule
from phasecalculator.classes.phase import Phase
from phasecalculator.classes.phases import Phases


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

NAME_INCHIKEY_MAP = pureinf.get_name_inchikey_mapping()
INCHIKEY_POLARISED_SSIP_FILE_DICT = pureinf.get_ssip_file_dict()
INCHIKEY_UNPOLARISED_SSIP_FILE_DICT = pureinf.get_unpolarised_ssip_file_dict()


def convert_csv_file_to_phases(csv_filename, unpolarised_ssips):
    """Read CSV file and convert contents to Phases.

    Parameters
    ----------
    csv_filename : str
        CSV filename.
    unpolarised_ssips : boolean
        True if unpolarised SSIP descriptions are to be used.

    Returns
    -------
    Phases
        Phases object.

    """
    csv_file_contents = read_csv_file(csv_filename)
    return create_phases(csv_file_contents, unpolarised_ssips)


def read_csv_file(csv_filename):
    """Read CSV file to list of rows.

    Parameters
    ----------
    csv_filename : str
        CSV filename.

    Returns
    -------
    list
        list of rows.

    """
    with open(csv_filename, "r") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter="\t")
        return [row for row in csv_reader]


def create_phases(csv_file_contents, unpolarised_ssips):
    """Create Phases for csv file contents

    Parameters
    ----------
    csv_file_contents : list of lists
        List of CSV file lines.
    unpolarised_ssips : boolean
        True if unpolarised SSIP descriptions are to be used.

    Returns
    -------
    Phases
        Phases object.

    """
    phase_list = []
    for csv_file_line in csv_file_contents:
        phase_list.append(create_phase(csv_file_line, unpolarised_ssips))
    return Phases(phase_list)


def create_phase(csv_file_line, unpolarised_ssips):
    """Create Phase from information list.
    

    Parameters
    ----------
    csv_file_line : list of str
        line from CSV file.
    unpolarised_ssips : boolean
        True if unpolarised SSIP descriptions are to be used.

    Returns
    -------
    Phase
        Phase object.

    """
    temperature = create_temperature(float(csv_file_line[0]))
    molecule_list = []
    for i in range(1, len(csv_file_line), 2):
        name = csv_file_line[i]
        molefraction = float(csv_file_line[i + 1])
        molecule_list.append(create_molecule(name, molefraction, unpolarised_ssips))
    return Phase(molecule_list, temperature)


def create_molecule(name, molefraction, unpolarised_ssips):
    """Create Molecule.

    Parameters
    ----------
    name : str
        Molecule name.
    molefraction : float
        mole fraction of molecule in phase.
    unpolarised_ssips : boolean
        True if unpolarised SSIP descriptions are to be used.

    Raises
    ------
    keyerr
        Name does not match.

    Returns
    -------
    Molecule
        Molecule object.

    """
    try:
        inchikey = NAME_INCHIKEY_MAP[name]
        if unpolarised_ssips:
            ssip_file_loc = INCHIKEY_UNPOLARISED_SSIP_FILE_DICT[inchikey]
        else:
            ssip_file_loc = INCHIKEY_POLARISED_SSIP_FILE_DICT[inchikey]
        return Molecule(name, inchikey, ssip_file_loc, molefraction)
    except KeyError as keyerr:
        LOGGER.error("Invalid entry: %s Not found.", name)
        raise keyerr


def create_temperature(temperature_value):
    """Create Temperature.

    Parameters
    ----------
    temperature_value : float
        value in Kelvin.

    Returns
    -------
    Temperature
        Temperature object.

    """
    return Temperature(temperature_value, "KELVIN")
