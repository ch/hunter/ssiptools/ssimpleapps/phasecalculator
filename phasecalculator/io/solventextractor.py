# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for spliting input fed into different energy XML files based on solvent ID.

@author: mark
"""

import logging
import solventmapcreator.io.solvationenergyextraction as solvextract

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def extract_and_write_energy_values_from_files(
    solvent_filename, energy_xml_filename, energy_type, directory
):
    """Extracts energies and splits to different file for each solvent.

    Parameters
    ----------
    solvent_filename : str
        Solvent XML filename.
    energy_xml_filename : str
        Energy XML filename.
    energy_type : str
        type of energy to extract. Either "binding" or "free".
    directory : str
        directory name.

    Returns
    -------
    filename_list : list of str
        filenames for files outputted to disk.

    """
    return solvextract.extract_and_write_energy_values_from_files(
        solvent_filename, energy_xml_filename, energy_type, directory
    )
