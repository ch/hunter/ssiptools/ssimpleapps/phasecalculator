# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for running polynomial generation.

@author: Mark
"""

import logging
import solventmapcreator.io.polynomialdatareader as polyread
import solventmapcreator.polynomialanalysis.polynomialplotting as polyplot

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


POLY_ORDER_LIST = [8]

POSITIVE_POINTS = ["{:.3f}solute".format((float(x) / 10.0)) for x in range(101)]

NEGATIVE_POINTS = ["{:.3f}solute".format((float(x) / 10.0) - 20.0) for x in range(201)]


def read_poly_data_to_dict(poly_filename_list, **kwargs):
    """Parse each file, and then returns the dictionary of information, by solvent ID.

    Parameters
    ----------
    poly_filename_list : list
        list of polynomial file names.
    suffix : str, optional
        polynomial file suffix.
    temperature_dir : bool, optional
        if files are in temperature based directories, indicates to include
        the preceding directory name in solvent ID to be able to distiguish
        the same solvent composition at different temperatures.

    Returns
    -------
    dict
        dict of polynomial data by solvent ID.

    """
    return polyread.parse_polynomial_data_file_list(poly_filename_list, **kwargs)


def generate_polynomial_data_free_energy_file_list(free_energy_filename_list):
    """Generate polynomial fits for free energy data in given files.

    Parameters
    ----------
    free_energy_filename_list : list of str
        List of energy XML filenames.

    Returns
    -------
    out_res_split : list
        List of results of polynomial file generation.
    filename_list : list of str
        List of polynomial filenames.

    """
    out_res_split = []
    filename_list = []
    for free_energy_filename in free_energy_filename_list:
        poly_filename_split = generate_polynomial_filename(
            free_energy_filename, suffix="_poly_fit_split.csv"
        )
        out_res_split.append(
            polyplot.parse_free_energy_poly_data_to_file_split_fit(
                free_energy_filename,
                POLY_ORDER_LIST,
                poly_filename_split,
                POSITIVE_POINTS,
                NEGATIVE_POINTS,
            )
        )
        filename_list.append(poly_filename_split)
    return out_res_split, filename_list


def generate_polynomial_data_binding_energy_file_list(binding_energy_filename_list):
    """Generate polynomial fits for binding energy data in given files.

    Parameters
    ----------
    binding_energy_filename_list : list of str
        List of energy XML filenames.

    Returns
    -------
    out_res_split : list
        List of results of polynomial file generation.
    filename_list : list of str
        List of polynomial filenames.

    """
    out_res_split = []
    filename_list = []
    for binding_energy_filename in binding_energy_filename_list:
        poly_filename_split = generate_polynomial_filename(
            binding_energy_filename, suffix="_poly_fit_split.csv"
        )
        out_res_split.append(
            polyplot.parse_binding_energy_poly_data_to_file_split_fit(
                binding_energy_filename,
                POLY_ORDER_LIST,
                poly_filename_split,
                POSITIVE_POINTS,
                NEGATIVE_POINTS,
            )
        )
        filename_list.append(poly_filename_split)
    return out_res_split, filename_list


def generate_polynomial_filename(free_energy_filename, suffix="_poly_fit.csv"):
    """Generate polynomial filename from energy XML filename.

    Parameters
    ----------
    free_energy_filename : str
        Energy XML filename.
    suffix : string, optional
        filename suffix. The default is '_poly_fit.csv'.

    Returns
    -------
    str
        polynomial filename.

    """
    return free_energy_filename.replace(".xml", suffix)
