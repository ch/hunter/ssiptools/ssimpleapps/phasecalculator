# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for IO handling of System Collection class.

@author: Mark
"""

import logging
from lxml import etree
import xmlvalidator.xmlvalidation as xmlval
from phasecalculator.classes.system import SystemCollection

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def read_system_collection_file(xml_filename):
    """Read SystemCollection from file.

    Parameters
    ----------
    xml_filename : str
        XML filename.

    Returns
    -------
    SystemCollection
        SystemCollection for calculations.

    """
    system_collection_etree = xmlval.validate_and_read_xml_file(
        xml_filename, xmlval.PHASE_CALCULATOR_SCHEMA
    )
    return SystemCollection.parse_xml(system_collection_etree)


def write_system_collection_file(system_collection, xml_filename):
    """Write SystemCollection to XML file.

    Parameters
    ----------
    system_collection : SystemCollection
        SystemCollection for calculations.
    xml_filename : str
        XML filename.

    Returns
    -------
    None.

    """
    sys_coll_element = system_collection.write_to_xml()
    system_coll_etree = etree.ElementTree(sys_coll_element)
    xmlval.validate_xml(system_coll_etree, xmlval.PHASE_CALCULATOR_SCHEMA)
    system_coll_etree.write(
        xml_filename, encoding="UTF-8", xml_declaration=True, pretty_print=True
    )
