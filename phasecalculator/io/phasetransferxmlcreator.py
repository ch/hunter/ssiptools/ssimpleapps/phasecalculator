# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Module for creating Solvent and Phase XML for phase transfer calculations.

@author: Mark
"""

import logging
import phasexmlcreator.multicomponentassembler as multiassem
import phasexmlcreator.phasexmlmaker as phasemake
import phasexmlcreator.solventxmlmaker as solvmake
import puresolventinformation.information as pureinfo

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def create_solvent_file(mole_fraction_dict_list, solvent_filename, **kwargs):
    """Create solvent XML file for given compositions.

    Parameters
    ----------
    mole_fraction_dict_list : list of dicts
        List of dictionaries of name: mole fraction pairs per solvent to
        create.
    solvent_filename : str
        Solvent XML output filename.
    solvent_info_filename : str, optional
        Filename for file containing solvent information. Defaults to file in
        phasexmlcreator module.
    ssip_filename_list : list of str, optional
        Filenames for SSIP XML files. Defaults to SSIP files in pureinformation
        module.
    name_inchikey_map : dict, optional
        inchikey: name pairs. Defaults to names of solvents in pureinformation
        module.

    Returns
    -------
    None.

    """
    solvent_info_list = generate_solv_info_list(mole_fraction_dict_list, **kwargs)
    write_solvent_file(solvent_info_list, solvent_filename)


def create_phase_file(mole_fraction_dict_list_by_temp, phase_filename, **kwargs):
    """Create phase XML file for given compositions and temperatures.

    Parameters
    ----------
    mole_fraction_dict_list_by_temp : list of lists
        List containing temperature: phase mole fractions pairs for phases to
        create XML for. Phase mole fractions contains a list of dictionaries
        of name: mole fraction pairs per phase to create. Temperature
        information with dictionaries containing value and unit.
    phase_filename : str
        Phase XML output filename.
    solvent_info_filename : str, optional
        Filename for file containing solvent information. Defaults to file in
        phasexmlcreator module.
    ssip_filename_list : list of str, optional
        Filenames for SSIP XML files. Defaults to SSIP files in pureinformation
        module.
    name_inchikey_map : dict, optional
        inchikey: name pairs. Defaults to names of solvents in pureinformation
        module.

    Returns
    -------
    None.

    """
    phase_info_by_temp = []
    for temperature_info, mole_fraction_dict_list in mole_fraction_dict_list_by_temp:
        phase_info_list = generate_solv_info_list(mole_fraction_dict_list, **kwargs)
        phase_info_by_temp.append([temperature_info, phase_info_list])
    write_phase_file(phase_info_by_temp, phase_filename)


def write_solvent_file(solvent_info_list, solvent_filename):
    """Write solvent XML file for given compositions.

    Parameters
    ----------
    solvent_info_list : list of dicts
        List of dictionaries of information to create XML for given solvent
        compositions.
    solvent_filename : str
        Output filename.

    Returns
    -------
    None.

    """
    solvmake.write_solvent_information_to_file(solvent_info_list, solvent_filename)


def write_phase_file(phase_info_list_by_temp, phase_filename):
    """Write phase XML file for given compositions and temperatures.

    Parameters
    ----------
    phase_info_list_by_temp : list of lists
        List of lists. Each element of top level list contains a temperature_info
        dictionary and a list of dictionaries of phase composition information.
        Temperature information dictionary contains temperature value and unit.
        This is to create XML for given phase compositions at the accompanying
        temperature.
    phase_filename : str
        Output filename.

    Returns
    -------
    None.

    """
    phasemake.write_phase_information_to_file_diff_temp(
        phase_info_list_by_temp, phase_filename
    )


def generate_solv_info_list(mole_fraction_dict_list, **kwargs):
    """Generate solvent info from read in SSIP files and solvent information.

    Parameters
    ----------
    mole_fraction_dict_list : list of dicts
        List of dictionaries of name: mole fraction pairs per phase/solvent to
        create.
    solvent_info_filename : str, optional
        Filename for file containing solvent information. Defaults to file in
        phasexmlcreator module.
    ssip_filename_list : list of str, optional
        Filenames for SSIP XML files. Defaults to SSIP files in pureinformation
        module.
    name_inchikey_map : dict, optional
        inchikey: name pairs. Defaults to names of solvents in pureinformation
        module.

    Returns
    -------
    solv_info_list : list of dicts
        List of dictionaries of information to create XML for given solvent
        compositions.

    """
    solvent_filename = kwargs.get(
        "solvent_info_filename",
        multiassem.solvconcreader.DEFAULT_SOLVENT_INFORMATION_FILE,
    )
    ssip_filename_list = kwargs.get(
        "ssip_filename_list", pureinfo.get_ssip_file_dict().values()
    )
    name_inchikey_map = kwargs.get(
        "name_inchikey_map", pureinfo.get_name_inchikey_mapping()
    )
    return multiassem.generate_solv_info_from_files(
        mole_fraction_dict_list, solvent_filename, ssip_filename_list, name_inchikey_map
    )
