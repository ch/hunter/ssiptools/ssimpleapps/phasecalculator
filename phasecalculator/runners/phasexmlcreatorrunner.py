# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for creating Solvent and phase XML for calculations.

@author: Mark
"""

import logging
import pathlib
import phasecalculator.io.phasetransferxmlcreator as phasexmlcreate

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def create_phase_and_solvent_files(system_info, filestem="system", **kwargs):
    """Create solvent and phase files for given system.

    Parameters
    ----------
    system_info : System
        System object.
    filestem : str, optional
        filename stem. The default is "system".
    solvent_info_filename : str, optional
        Filename for file containing solvent information. Defaults to file in
        phasexmlcreator module.
    name_inchikey_map : dict, optional
        inchikey: name pairs. Defaults to names of solvents in pureinformation
        module.


    Returns
    -------
    phase_file : str
        Phase filename.
    solvent_list : list of dict
        List of dictionaries containing solvent filename and temperature information.

    """
    create_scratch_dir(system_info)
    name_inchikey_map = kwargs.pop(
        "name_inchikey_map", system_info.get_name_inchikey_map()
    )
    phase_file = None
    solvent_list = None
    LOGGER.info(
        "System calcs: VLE: %s, FGIP: %s, SIM: %s",
        system_info.calc_vle(),
        system_info.calc_fgip(),
        system_info.calc_similarity(),
    )
    if system_info.calc_vle():
        phase_file = create_phase_file(
            system_info,
            filestem + "phase",
            name_inchikey_map=name_inchikey_map,
            **kwargs
        )
    if system_info.calc_fgip() or system_info.calc_similarity():
        LOGGER.info("Creating solvents")
        solvent_list = create_solvent_files(
            system_info,
            filestem + "solvent",
            name_inchikey_map=name_inchikey_map,
            **kwargs
        )
    return phase_file, solvent_list


def create_phase_file(system_info, filestem, **kwargs):
    """Create phase file for input information.

    Parameters
    ----------
    system_info : System
        System object.
    filestem : str
        filename stem.
    solvent_info_filename : str, optional
        Filename for file containing solvent information. Defaults to file in
        phasexmlcreator module.
    name_inchikey_map : dict, optional
        inchikey: name pairs. Defaults to names of solvents in pureinformation
        module.

    Returns
    -------
    phase_filename : str
        phase filename.

    """
    phase_filename = create_phase_filename(
        system_info.runtime_information.scratch_dir, filestem
    )
    mole_fractions_by_temp_dict = system_info.get_phases_by_temp_list()
    ssip_filename_list = system_info.get_ssip_file_locations()
    phasexmlcreate.create_phase_file(
        mole_fractions_by_temp_dict,
        phase_filename,
        ssip_filename_list=ssip_filename_list,
        **kwargs
    )
    return phase_filename


def create_solvent_files(system_info, filestem, **kwargs):
    """Create solvent files for input information.

    Parameters
    ----------
    system_info : System
        System object.
    filestem : str
        filename stem.
    solvent_info_filename : str, optional
        Filename for file containing solvent information. Defaults to file in
        phasexmlcreator module.
    name_inchikey_map : dict, optional
        inchikey: name pairs. Defaults to names of solvents in pureinformation
        module.

    Returns
    -------
    solvent_list : list of dict
        List of dictionaries containing solvent filename and temperature information.

    """
    mole_fractions_by_temp_dict = system_info.get_phase_compositions_by_temperature()
    ssip_filename_list = system_info.get_ssip_file_locations()
    solvent_list = []
    LOGGER.info("SSIP filename list: %s", ssip_filename_list)
    for temp_info, mole_fraction_dict_list in mole_fractions_by_temp_dict.items():
        solvent_filename = create_solvent_file(
            mole_fraction_dict_list,
            ssip_filename_list,
            temp_info,
            filestem,
            system_info.runtime_information.scratch_dir,
            **kwargs
        )
        solvent_list.append(
            {"solvent_filename": solvent_filename, **temp_info.to_dict()}
        )
    return solvent_list


def create_solvent_file(
    mole_fraction_dict_list,
    ssip_filename_list,
    temp_info,
    filestem,
    scratch_dir,
    **kwargs
):
    """Create solvent file for input information.

    Parameters
    ----------
    mole_fraction_dict_list : list of dict
        Solvent compositions.
    ssip_filename_list : list of str
        SSIP filenames for molecules.
    temp_info : Temperature
        Temperature object.
    filestem : str
        filename stem.
    scratch_dir : str
        scratch directory name.
    solvent_info_filename : str, optional
        Filename for file containing solvent information. Defaults to file in
        phasexmlcreator module.
    name_inchikey_map : dict, optional
        inchikey: name pairs. Defaults to names of solvents in pureinformation
        module.

    Returns
    -------
    solvent_filename : str
        filename path including scratch directory.

    """
    solvent_filename = create_solvent_filename(scratch_dir, temp_info, filestem)
    phasexmlcreate.create_solvent_file(
        mole_fraction_dict_list,
        solvent_filename,
        ssip_filename_list=ssip_filename_list,
        **kwargs
    )
    return solvent_filename


def create_scratch_dir(system_info):
    """Makes scratch directory for system calculation. 

    Parameters
    ----------
    system_info : System
        Calculation System object.

    Returns
    -------
    None.

    """
    scratch_path = pathlib.Path(system_info.runtime_information.scratch_dir)
    scratch_path.mkdir(parents=True, exist_ok=True)


def create_solvent_filename(scratch_dir, temp_info, filestem):
    """Create solvent filename.

    Parameters
    ----------
    scratch_dir : str
        scratch directory name.
    temp_info : Temperature
        Temperature object.
    filestem : str
        filename stem.

    Returns
    -------
    path : str
        filename path including scratch directory.

    """
    temp_string = create_temperaturestring(
        temp_info.temperature, temp_info.temperature_unit
    )
    filename = filestem + "_" + temp_string + ".xml"
    path = (pathlib.Path(scratch_dir) / filename).as_posix()
    return path


def create_phase_filename(scratch_dir, filestem):
    """Create filename for phase XML.

    Parameters
    ----------
    scratch_dir : str
        scratch directory name.
    filestem : str
        filename stem.

    Returns
    -------
    path : str
        filename path including scratch directory.

    """
    filename = filestem + ".xml"
    path = (pathlib.Path(scratch_dir) / filename).as_posix()
    return path


def create_temperaturestring(temperature, temperature_unit):
    """Write temperature information in formatted string for use in filenames.

    Parameters
    ----------
    temperature : float
        Temperature value.
    temperature_unit : str
        Temperature unit.

    Returns
    -------
    str
        temperature string.

    """
    return "{:.1f}{}".format(temperature, "K" if temperature_unit == "KELVIN" else "C")
