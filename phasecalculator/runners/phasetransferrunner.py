# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for running phasetransfer calculations.

@author: Mark
"""

import logging
import subprocess
import pathlib
import sys
import puresolventinformation.information as pureinf

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def run_vle_calculation(jar_path, phase_filename, output_filename, **kwargs):
    """Run VLE phase calculation.

    Parameters
    ----------
    jar_path : str
        Path to jar executable.
    phase_filename : str
        Filename for phase XML.
    output_filename : str
        Output filename for result output.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.

    Raises
    ------
    FileNotFoundError
        If jar file is not found.

    Returns
    -------
    CompletedProcess
        result of system call.

    """
    if check_jar_exists(jar_path):
        vle_args = generate_vle_calc_args(
            jar_path, phase_filename, output_filename, **kwargs
        )
        LOGGER.info(f"phasetransfer args: {vle_args}")
        return run_calculation(vle_args)
    else:
        raise FileNotFoundError(f"File does not exist: {jar_path}")


def run_phasetransfer_binding_energy(
    jar_path, output_filename, frac_occ_filename, solvent_filename, **kwargs
):
    """Run solute solvation binding energy calculation.

    Parameters
    ----------
    jar_path : str
        path to jar executable.
    output_filename : str
        Output filename for result output.
    frac_occ_filename: str
        Filename for fractional Occupancy XML.
    solvent_filename : str
        filename for solvent XML.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.
    temperature : float, optional
        Temperature value for calculation. The default is 298.0.
    temperature_unit : str, optional
        Specify temperature unit: either "KELVIN" or "CELSIUS" . The default is "KELVIN".
    solute_filename : str, optional
        Solute filename. The default is pureinf.SINGLE_SSIP_SOLUTE_FILE.


    Raises
    ------
    FileNotFoundError
        If jar file is not found.

    Returns
    -------
    CompletedProcess
        result of system call.

    """
    if check_jar_exists(jar_path):
        phase_binding_args = generate_phasetransfer_binding_args(
            jar_path, output_filename, frac_occ_filename, solvent_filename, **kwargs
        )
        LOGGER.info("phasetransfer args: %s", phase_binding_args)
        return run_calculation(phase_binding_args)
    else:
        raise FileNotFoundError(f"File does not exist: {jar_path}")


def run_phasetransfer_free_energy(
    jar_path, output_filename, solvent_filename, **kwargs
):
    """Run solute solvation free energy calculation.

    Parameters
    ----------
    jar_path : str
        path to jar executable.
    output_filename : str
        Output filename for result output.
    solvent_filename : str
        filename for solvent XML.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.
    temperature : float, optional
        Temperature value for calculation. The default is 298.0.
    temperature_unit : str, optional
        Specify temperature unit: either "KELVIN" or "CELSIUS" . The default is "KELVIN".
    solute_filename : str, optional
        Solute filename. The default is pureinf.SINGLE_SSIP_SOLUTE_FILE.

    Raises
    ------
    FileNotFoundError
        If jar file is not found.

    Returns
    -------
    CompletedProcess
        result of system call.

    """
    if check_jar_exists(jar_path):
        phase_free_args = generate_phasetransfer_free_args(
            jar_path, output_filename, solvent_filename, **kwargs
        )
        LOGGER.info("phasetransfer args: %s", phase_free_args)
        return run_calculation(phase_free_args)
    else:
        raise FileNotFoundError(f"File does not exist: {jar_path}")


def run_calculation(arg_list):
    """Run calculation with given arguments.

    Parameters
    ----------
    arg_list : list
        list of args for command line program call.

    Returns
    -------
    CompletedProcess
        result of system call.

    """
    return subprocess.run(arg_list, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)


def generate_vle_calc_args(jar_path, phase_filename, output_filename, **kwargs):
    """Generate full argument list for VLE phase calculation.

    Parameters
    ----------
    jar_path : str
        Path to jar executable.
    phase_filename : str
        Filename for phase XML.
    output_filename : str
        Output filename for result output.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.

    Returns
    -------
    list
        argument list for phasetransfer jar call.

    """
    jar_args = make_java_arguments(jar_path, memory_req=kwargs.pop("memory_req", None))
    phase_calc_args = generate_phase_calc_args(phase_filename, output_filename)
    return jar_args + phase_calc_args


def generate_phasetransfer_free_args(
    jar_path, output_filename, solvent_filename, **kwargs
):
    """Generate full argument list for solute solvation free energy calculation call.

    Parameters
    ----------
    jar_path : str
        path to jar executable.
    output_filename : str
        Output filename for result output.
    solvent_filename : str
        filename for solvent XML.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.
    temperature : float, optional
        Temperature value for calculation. The default is 298.0.
    temperature_unit : str, optional
        Specify temperature unit: either "KELVIN" or "CELSIUS" . The default is "KELVIN".
    solute_filename : str, optional
        Solute filename. The default is pureinf.SINGLE_SSIP_SOLUTE_FILE.

    Returns
    -------
    list
        argument list for phasetransfer jar call.

    """
    jar_args = make_java_arguments(jar_path, memory_req=kwargs.pop("memory_req", None))
    free_args = ["--energies", "solvationtotal"]
    solvent_args = generate_solvent_calc_args(
        output_filename, solvent_filename, **kwargs
    )
    return jar_args + free_args + solvent_args


def generate_phasetransfer_binding_args(
    jar_path, output_filename, frac_occ_filename, solvent_filename, **kwargs
):
    """Generate full argument list for solute solvation binding energy calculation call.

    Parameters
    ----------
    jar_path : str
        path to jar executable.
    output_filename : str
        Output filename for binding energy XML.
    frac_occ_filename: str
        Filename for fractional Occupancy XML.
    solvent_filename : str
        filename for solvent XML.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.
    temperature : float, optional
        Temperature value for calculation. The default is 298.0.
    temperature_unit : str, optional
        Specify temperature unit: either "KELVIN" or "CELSIUS" . The default is "KELVIN".
    solute_filename : str, optional
        Solute filename. The default is pureinf.SINGLE_SSIP_SOLUTE_FILE.

    Returns
    -------
    list
        argument list for phasetransfer jar call.

    """
    jar_args = make_java_arguments(jar_path, memory_req=kwargs.pop("memory_req", None))
    binding_args = ["--energies", "solvationbinding", "-f", frac_occ_filename]
    solvent_args = generate_solvent_calc_args(
        output_filename, solvent_filename, **kwargs
    )
    return jar_args + binding_args + solvent_args


def generate_phase_calc_args(phase_filename, output_filename):
    """Generate argument list for phasetransfer VLE phase call.

    Parameters
    ----------
    phase_filename : str
        input phase XML filename.
    output_filename : str
        output XML filename.

    Returns
    -------
    list
        argument calls for jar file.

    """
    return [
        "--phaseOut",
        "--phaseCollectionList",
        "--conccalc",
        "--gascalc",
        "--calc",
        "--phaseFile",
        phase_filename,
        "-o",
        output_filename,
    ]


def generate_solvent_calc_args(
    output_filename,
    solvent_filename,
    temperature=298.0,
    temperature_unit="KELVIN",
    solute_filename=pureinf.SINGLE_SSIP_SOLUTE_FILE,
):
    """Generate list of arguments for solvation calculation.
    
    for solvent/solute.

    Parameters
    ----------
    output_filename : str
        Output filename for result output.
    solvent_filename : str
        filename for solvent XML.
    temperature : float, optional
        Temperature value for calculation. The default is 298.0.
    temperature_unit : str, optional
        Specify temperature unit: either "KELVIN" or "CELSIUS" . The default is "KELVIN".
    solute_filename : str, optional
        Solute filename. The default is pureinf.SINGLE_SSIP_SOLUTE_FILE.

    Returns
    -------
    list
        list of arguments for calculation related to solvent calculation.

    """
    return [
        "--calc",
        "--conccalc",
        "-o",
        output_filename,
        "--solvent",
        solvent_filename,
        "--solute",
        solute_filename,
        "--temperatureUnit",
        temperature_unit,
        "-t",
        f"{temperature:.1f}",
    ]


def make_java_arguments(jar_path, memory_req=None):
    """Create java runtime arguments.

    Parameters
    ----------
    jar_path : str
        jar filename string.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.

    Returns
    -------
    TYPE
        DESCRIPTION.

    """
    mem_options = []
    if memory_req is not None:
        mem_options = [f"-Xms{memory_req}", f"-Xmx{memory_req}"]
    return ["java"] + mem_options + ["-jar", jar_path]


def check_jar_exists(jar_path):
    """Check to see if the jar_path given corresponds to a file.

    Parameters
    ----------
    jar_path : str
        jar filename string.

    Returns
    -------
    boolean
        jar_path is file.

    """
    jar_file_path = pathlib.Path(jar_path)
    return jar_file_path.exists()
