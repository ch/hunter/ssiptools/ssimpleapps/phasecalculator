# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for running VLE analysis.

@author: Mark
"""

import logging
import phasecalculator.analysis.vleanalysis as vleanalysis
import phasecalculator.runners.phasetransferrunner as phaserun

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def calculate_and_process_vle_data(
    jar_path, phase_filename, phase_output_filename, csv_filename, **kwargs
):
    """Calculate and process VLE data.

    Parameters
    ----------
    jar_path : str
        Path to jar executable.
    phase_filename : str
        Filename for phase XML.
    phase_output_filename : str
        Output filename for result output.
    csv_filename : str
        CSV filename for output information.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.

    Raises
    ------
    FileNotFoundError
        If jar file is not found.
    

    Returns
    -------
    None.

    """
    run_vle_calculation(jar_path, phase_filename, phase_output_filename, **kwargs)
    process_vle_data(phase_output_filename, csv_filename)


def process_vle_data(calculated_phase_filename, csv_filename):
    """Process phase XML and write to csv file.

    Parameters
    ----------
    phase_filename : str
        Phase XML filename.
    csv_filename : str
        CSV filename for output information.

    Returns
    -------
    None.

    """
    return vleanalysis.process_vle_data(calculated_phase_filename, csv_filename)


def run_vle_calculation(jar_path, phase_filename, output_filename, **kwargs):
    """Run VLE phase calculation.

    Parameters
    ----------
    jar_path : str
        Path to jar executable.
    phase_filename : str
        Filename for phase XML.
    output_filename : str
        Output filename for result output.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.

    Raises
    ------
    FileNotFoundError
        If jar file is not found.

    Returns
    -------
    CompletedProcess
        result of system call.

    """
    return phaserun.run_vle_calculation(
        jar_path, phase_filename, output_filename, **kwargs
    )
