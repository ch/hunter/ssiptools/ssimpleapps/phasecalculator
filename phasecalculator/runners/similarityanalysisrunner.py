# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for running similarity analysis.

@author: Mark
"""

import logging
import pathlib
import phasecalculator.runners.phasetransferrunner as phaserun
import phasecalculator.analysis.similarityanalysis as simanalysis
import phasecalculator.io.solventextractor as solvextract
import phasecalculator.io.polynomialio as polyio

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def run_similarity_analysis(
    poly_filename_list, output_type, output_dir, output_filename="similaritymatrix.csv"
):
    """Run similarity analysis.

    Parameters
    ----------
    poly_filename_list : list of str
        list of polynomial filenames.
    output_type : str
        Output type. Three options: "all", "mixturecomparison" or "purecomparison".
    output_dir : str
        output directory for results.
    output_filename : str, optional
        output file location for similarity matrix. The default is
        "similaritymatrix.csv".

    Returns
    -------
    None.

    """
    if output_type == "all":
        simanalysis.compare_all_solvents(
            poly_filename_list, output_dir, output_filename
        )
    elif output_type == "mixturecomparison":
        simanalysis.compare_solvent_list(
            poly_filename_list, output_dir, output_filename
        )
    elif output_type == "purecomparison":
        simanalysis.compare_with_pure_solvents(poly_filename_list, output_dir)


def extract_all_solvents_and_generate_polynomials(
    solv_energy_dict_list, directory_base
):
    """Extract results for all solvents, and partition results based on temperature.

    Parameters
    ----------
    solv_energy_dict_list : list of dicts
        Solvation energy files .
    directory_base : str
        Base output directory.

    Returns
    -------
    combined_out_res : list
        List of polynomial generation results.
    combined_filename_list : list of str
        Polynomial filename list.

    """
    combined_out_res = []
    combined_filename_list = []
    for solv_energy_dict in solv_energy_dict_list:
        directory = generate_directory(
            directory_base,
            solv_energy_dict["temperature_value"],
            solv_energy_dict["temperature_units"],
        )
        solvent_filename = solv_energy_dict["solvent_filename"]
        energy_xml_filename = solv_energy_dict["energy_filename"]
        out_res, filename_list = extract_solvents_and_generate_polynomials(
            solvent_filename, energy_xml_filename, directory
        )
        combined_out_res.extend(out_res)
        combined_filename_list.extend(filename_list)
    return combined_out_res, combined_filename_list


def generate_directory(directory_base, temperature, temperature_unit):
    """Generate directory to put information based on temperature of simulation.
    
    Contain in parent directory, specified by directory_base, so calculation
    results are split up by temperature values.

    Parameters
    ----------
    directory_base : str
        base directory to put all information.
    temperature : float
        temperature of simulation.
    temperature_unit : str
        Unit for temperature. Either "KELVIN" or "CELSIUS".

    Returns
    -------
    str
        posix str for resulting directory created.
        

    """
    temperature_name = "{:.1f}{}".format(
        temperature, "K" if temperature_unit == "KELVIN" else "C"
    ).replace(".", "_")
    dir_path = pathlib.Path(directory_base) / temperature_name
    dir_path.mkdir(parents=True, exist_ok=True)
    return dir_path.as_posix()


def extract_solvents_and_generate_polynomials(
    solvent_filename, energy_xml_filename, directory
):
    """Extract solvation free energies and calculate free energy polynomials per solvent.

    Parameters
    ----------
    solvent_filename : str
        filename for solvent XML.
    energy_xml_filename : str
        energy XML filename.
    directory : str
        Directory to put files.

    Returns
    -------
    out_res, filename_list : list, list of str
        List of results for polynomial calculations,
        list of polynomial filenames.

    """
    energy_xml_filenames = solvextract.extract_and_write_energy_values_from_files(
        solvent_filename, energy_xml_filename, "free", directory
    )
    return polyio.generate_polynomial_data_free_energy_file_list(energy_xml_filenames)


def run_free_energy_calculation(jar_path, output_filename, solvent_filename, **kwargs):
    """Run solute solvation free energy calculation.

    Parameters
    ----------
    jar_path : str
        path to jar executable.
    output_filename : str
        Output filename for result output.
    solvent_filename : str
        filename for solvent XML.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.
    temperature : float, optional
        Temperature value for calculation. The default is 298.0.
    temperature_unit : str, optional
        Specify temperature unit: either "KELVIN" or "CELSIUS" . The default is "KELVIN".
    solute_filename : str, optional
        Solute filename. The default is pureinf.SINGLE_SSIP_SOLUTE_FILE.


    Raises
    ------
    FileNotFoundError
        If jar file is not found.

    Returns
    -------
    CompletedProcess
        result of system call.

    """
    return phaserun.run_phasetransfer_free_energy(
        jar_path, output_filename, solvent_filename, **kwargs
    )
