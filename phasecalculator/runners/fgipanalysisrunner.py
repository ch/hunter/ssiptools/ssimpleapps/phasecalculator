# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script with methods for running FGIP analysis on solvent XML file.

@author: Mark
"""

import logging
import pathlib
import phasecalculator.runners.phasetransferrunner as phaserun
import phasecalculator.analysis.fgipanalysis as fgipanalysis
import phasecalculator.io.solventextractor as solvextract
import phasecalculator.io.polynomialio as polyio


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def calc_energies_and_fgips(
    jar_path,
    energy_xml_filename,
    frac_occ_filename,
    solvent_filename,
    directory_base,
    **kwargs
):
    """Run phasetransfer calculation and generate FGIPs for input solvent.

    Parameters
    ----------
    jar_path : str
        path to jar file.
    energy_xml_filename : str
        energy XML filename.
    frac_occ_filename: str
        Filename for fractional Occupancy XML.
    solvent_filename : str
        Solvent XML filename.
    directory_base : str
        base directory name.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.
    temperature : float, optional
        Temperature value for calculation. The default is 298.0.
    temperature_unit : str, optional
        Specify temperature unit: either "KELVIN" or "CELSIUS" . The default is "KELVIN".
    **kwargs : TYPE
        DESCRIPTION.

    Returns
    -------
    results, latex_block : list, str
        List of results of calculation outcomes, and latex text block for figures.

    """
    temperature = kwargs.get("temperature", 298.0)
    temperature_unit = kwargs.get("temperature_unit", "KELVIN")
    run_binding_energy_calculation(
        jar_path, energy_xml_filename, frac_occ_filename, solvent_filename, **kwargs
    )
    directory = generate_directory(directory_base, temperature, temperature_unit)
    out_res, polynomial_filename_list = extract_solvents_and_generate_polynomials(
        solvent_filename, energy_xml_filename, directory
    )
    return run_fgip_analysis(
        frac_occ_filename, polynomial_filename_list, directory, **kwargs
    )


def generate_directory(directory_base, temperature, temperature_unit):
    """Generate directory to put information based on temperature of simulation.
    
    Contain in parent directory, specified by directory_base, so calculation
    results are split up by temperature values.

    Parameters
    ----------
    directory_base : str
        base directory to put all information.
    temperature : float
        temperature of simulation.
    temperature_unit : str
        Unit for temperature. Either "KELVIN" or "CELSIUS".

    Returns
    -------
    str
        posix str for resulting directory created.
        

    """
    temperature_name = "{:.1f}{}".format(
        temperature, "K" if temperature_unit == "KELVIN" else "C"
    ).replace(".", "_")
    dir_path = pathlib.Path(directory_base) / temperature_name
    dir_path.mkdir(parents=True, exist_ok=True)
    return dir_path.as_posix()


def run_fgip_analysis(frac_occ_filename, polynomial_filename_list, directory, **kwargs):
    """Run FGIP analysis for all solvents included in solvent file.

    Parameters
    ----------
    frac_occ_filename: str
        Filename for fractional occupancy XML.
    polynomial_filename_list : list of str
        list of polynomial files to read.
    directory : str
        posix string of directory to deposit files.
    **kwargs : TYPE
        DESCRIPTION.

    Returns
    -------
    results, latex_block : list, str
        List of results of calculation outcomes, and latex text block for figures.

    """
    return fgipanalysis.create_solv_map_and_fgips_from_files(
        frac_occ_filename, polynomial_filename_list, directory, **kwargs
    )


def extract_solvents_and_generate_polynomials(
    solvent_filename, energy_xml_filename, directory
):
    """Extract solvation binding energies and calculate binding polynomials per solvent.

    Parameters
    ----------
    solvent_filename : str
        filename for solvent XML.
    energy_xml_filename : str
        energy XML filename.
    directory : str
        Directory to put files.

    Returns
    -------
    out_res, filename_list : list, list of str
        List of results for polynomial calculations,
        list of polynomial filenames.

    """
    energy_xml_filenames = solvextract.extract_and_write_energy_values_from_files(
        solvent_filename, energy_xml_filename, "binding", directory
    )
    return polyio.generate_polynomial_data_binding_energy_file_list(
        energy_xml_filenames
    )


def run_binding_energy_calculation(
    jar_path, output_filename, frac_occ_filename, solvent_filename, **kwargs
):
    """Run solute solvation binding energy calculation.

    Parameters
    ----------
    jar_path : str
        path to jar executable.
    output_filename : str
        Output filename for result output.
    frac_occ_filename: str
        Filename for fractional Occupancy XML.
    solvent_filename : str
        filename for solvent XML.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.
    temperature : float, optional
        Temperature value for calculation. The default is 298.0.
    temperature_unit : str, optional
        Specify temperature unit: either "KELVIN" or "CELSIUS" . The default is "KELVIN".
    solute_filename : str, optional
        Solute filename. The default is pureinf.SINGLE_SSIP_SOLUTE_FILE.


    Raises
    ------
    FileNotFoundError
        If jar file is not found.

    Returns
    -------
    CompletedProcess
        result of system call.

    """
    return phaserun.run_phasetransfer_binding_energy(
        jar_path, output_filename, frac_occ_filename, solvent_filename, **kwargs
    )
