# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for running phase calculator calculations.

@author: Mark
"""

import logging
import pathlib
import phasecalculator.runners.phasexmlcreatorrunner as phasexmlrun
import phasecalculator.runners.fgipanalysisrunner as fgiprun
import phasecalculator.runners.similarityanalysisrunner as simrun
import phasecalculator.runners.vleanalysisrunner as vlerun


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def run_all_analysis(system_info, **kwargs):
    """Generate required solvent and phase XML then run all relevant analysis.

    Parameters
    ----------
    system_info : System
        System information.
    output_filename : str, optional
        output file location for similarity matrix. The default is
        "similaritymatrix.csv".
    memory_req : str, optional
        Memory settings for jar executable. The default is None.

    Returns
    -------
    None.

    """
    LOGGER.info("Creating phase and solvent filename lists")
    phase_filename, solvent_list = create_phase_and_solvent_xml_files(
        system_info, **kwargs
    )
    LOGGER.info("Phase filename: %s", phase_filename)
    LOGGER.info("Solvent filename list: %s", solvent_list)
    if system_info.calc_vle():
        LOGGER.info("Running VLE calc")
        run_vle_analysis(system_info, phase_filename, **kwargs)
    if system_info.calc_fgip():
        LOGGER.info("Running FGIP calc")
        run_fgip_analysis(system_info, solvent_list, **kwargs)
    if system_info.calc_similarity():
        LOGGER.info("Running similarity calc")
        run_similarity_analysis(system_info, solvent_list, **kwargs)


def run_fgip_analysis(system_info, solvent_filename_dict_list, **kwargs):
    """Run FGIP analysis.

    Parameters
    ----------
    system_info : System
        System information.
    solvent_filename_dict_list : dict
        dictionary of solvent filenames with temperature information.
    output_filename : str, optional
        output file location for similarity matrix. The default is
        "similaritymatrix.csv".
    memory_req : str, optional
        Memory settings for jar executable. The default is None.

    Returns
    -------
    combined_results : list
        combined results of FGIP generation.
    combined_latex_blocks : str
        combined latex blocks for figures.

    """
    # TODO: for each solvent file/temp combo in dict
    jar_path = system_info.runtime_information.phasetransfer_jar
    combined_results = []
    combined_latex_blocks = ""
    for solvent_filename_dict in solvent_filename_dict_list:
        solvent_filename = solvent_filename_dict["solvent_filename"]
        energy_xml_filename = create_binding_energy_output_filename(solvent_filename)
        frac_occ_filename = create_fractional_occupancy_filename(solvent_filename)
        directory_base = system_info.runtime_information.output_dir
        temperature = solvent_filename_dict["temperature_value"]
        temperature_unit = solvent_filename_dict["temperature_units"]
        results, latex_blocks = fgiprun.calc_energies_and_fgips(
            jar_path,
            energy_xml_filename,
            frac_occ_filename,
            solvent_filename,
            directory_base,
            temperature=temperature,
            temperature_unit=temperature_unit,
            **kwargs
        )
        combined_results.extend(results)
        combined_latex_blocks += latex_blocks
    return combined_results, combined_latex_blocks


def run_similarity_analysis(system_info, solvent_filename_dict, **kwargs):
    """Run similarity analysis.

    Parameters
    ----------
    system_info : System
        System information.
    solvent_filename_dict : dict
        dictionary of solvent filenames with temperature information.
    output_filename : str, optional
        output file location for similarity matrix. The default is
        "similaritymatrix.csv".
    memory_req : str, optional
        Memory settings for jar executable. The default is None.

    Returns
    -------
    None.

    """
    solvent_filename_dict_list = []
    jar_path = system_info.runtime_information.phasetransfer_jar
    for solv_dict in solvent_filename_dict:
        solvent_filename = solv_dict["solvent_filename"]
        energy_filename = create_free_energy_output_filename(solvent_filename)
        temperature = solv_dict["temperature_value"]
        temperature_unit = solv_dict["temperature_units"]
        solv_dict["energy_filename"] = energy_filename
        solvent_filename_dict_list.append(solv_dict)
        simrun.run_free_energy_calculation(
            jar_path,
            energy_filename,
            solvent_filename,
            temperature=temperature,
            temperature_unit=temperature_unit,
            **kwargs
        )
    output_dir = system_info.runtime_information.output_dir
    output_type = system_info.output_information.similarity_output_type
    out_res, poly_filename_list = simrun.extract_all_solvents_and_generate_polynomials(
        solvent_filename_dict_list, output_dir
    )
    simrun.run_similarity_analysis(
        poly_filename_list, output_type, output_dir, **kwargs
    )


def run_vle_analysis(system_info, phase_filename, **kwargs):
    """Run VLE phase calculation.

    Parameters
    ----------
    system_info : System
        System information.
    phase_filename : str
        Filename for phase XML.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.

    Raises
    ------
    FileNotFoundError
        If jar file is not found.

    Returns
    -------
    CompletedProcess
        result of system call.

    """
    jar_path = system_info.runtime_information.phasetransfer_jar
    phase_output_filename = create_phase_output_filename(phase_filename)
    csv_filename = create_phase_summary_filename(
        system_info.runtime_information.output_dir, phase_filename
    )
    create_output_dir(system_info)
    vlerun.calculate_and_process_vle_data(
        jar_path, phase_filename, phase_output_filename, csv_filename, **kwargs
    )


def create_output_dir(system_info):
    """Makes output directory for system calculation. 

    Parameters
    ----------
    system_info : System
        Calculation System object.

    Returns
    -------
    None.

    """
    output_path = pathlib.Path(system_info.runtime_information.output_dir)
    output_path.mkdir(parents=True, exist_ok=True)
    return output_path.as_posix()


def create_phase_summary_filename(output_dir, phase_filename):
    """Create phase summary CSV filename.

    Parameters
    ----------
    output_dir : str
        output directory name.
    phase_filename : str
        phase filename.

    Returns
    -------
    str
        CSV filename.

    """
    summary_filename = phase_filename.split("/")[-1].replace(
        ".xml", "calculation_summary.csv"
    )
    return (pathlib.Path(output_dir) / summary_filename).as_posix()


def create_free_energy_output_filename(solvent_filename):
    """Create free energy filename for solvent file.

    Parameters
    ----------
    solvent_filename : str
        Input solvent filename.

    Returns
    -------
    str
        Output energy filename.

    """
    return solvent_filename.replace(".xml", "free.xml")


def create_binding_energy_output_filename(solvent_filename):
    """Create binding energy filename for solvent file.

    Parameters
    ----------
    solvent_filename : str
        Input solvent filename.

    Returns
    -------
    str
        Output energy filename.

    """
    return solvent_filename.replace(".xml", "binding.xml")


def create_fractional_occupancy_filename(solvent_filename):
    """Create fractional occupancy filename for solvent file.

    Parameters
    ----------
    solvent_filename : str
        Input solvent filename.

    Returns
    -------
    str
        Fractional occupancy filename.

    """
    return solvent_filename.replace(".xml", "_fracocc.xml")


def create_phase_output_filename(phase_filename):
    """Create phase output filename.

    Parameters
    ----------
    phase_filename : str
        Input phase filename.

    Returns
    -------
    str
        phase output filename.

    """
    return phase_filename.replace(".xml", "calculated.xml")


def create_phase_and_solvent_xml_files(system_info, **kwargs):
    """Create solvent and phase files for given system.

    Parameters
    ----------
    system_info : System
        System object.
    filestem : str, optional
        filename stem. The default is "system".
    solvent_info_filename : str, optional
        Filename for file containing solvent information. Defaults to file in
        phasexmlcreator module.
    name_inchikey_map : dict, optional
        inchikey: name pairs. Defaults to names of solvents in pureinformation
        module.


    Returns
    -------
    phase_file : str
        Phase filename.
    solvent_list : list of dict
        List of dictionaries containing solvent filename and temperature information.


    """
    return phasexmlrun.create_phase_and_solvent_files(system_info, **kwargs)
