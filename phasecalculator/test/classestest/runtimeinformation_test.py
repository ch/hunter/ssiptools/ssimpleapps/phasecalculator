# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for runtime information tests.

@author: Mark
"""

import logging
import unittest
import pathlib
from lxml import etree
from phasecalculator.classes.runtimeinformation import RuntimeInformation

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class RuntimeInformationTestCase(unittest.TestCase):
    """Test case for methods."""

    def setUp(self):
        """Set up for tests.

        Returns
        -------
        None.

        """
        self.example_jar = "resources/example.jar"
        self.runtime_inf = RuntimeInformation(self.example_jar, "scratch", "fgip")

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        del self.example_jar
        del self.runtime_inf

    def test_parse_xml(self):
        """Test expected runtime information found.

        Returns
        -------
        None.

        """
        actual_inf = RuntimeInformation.parse_xml(self.runtime_inf.write_to_xml())
        self.assertEqual(self.runtime_inf, actual_inf)

    def test_parse_phasetransfer_jar(self):
        """Test expected Jar is found.

        Returns
        -------
        None.

        """
        actual_jar = RuntimeInformation.parse_phasetransfer_jar(
            self.runtime_inf.write_to_xml()
        )
        self.assertEqual(self.example_jar, actual_jar)

    def test_parse_scratch_dir(self):
        """Test expected scratch directory is found.

        Returns
        -------
        None.

        """
        expected_dir = "scratch"
        actual_dir = RuntimeInformation.parse_scratch_dir(
            self.runtime_inf.write_to_xml()
        )
        self.assertEqual(expected_dir, actual_dir)

    def test_parse_output_dir(self):
        """Test expected output directory is found.

        Returns
        -------
        None.

        """
        expected_dir = "fgip"
        actual_dir = RuntimeInformation.parse_output_dir(
            self.runtime_inf.write_to_xml()
        )
        self.assertEqual(expected_dir, actual_dir)

    def test_write_to_xml(self):
        """Test expected XML produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:RuntimeInformation xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">
  <phasecalc:PhaseTransferJar>resources/example.jar</phasecalc:PhaseTransferJar>
  <phasecalc:ScratchDirectory>scratch</phasecalc:ScratchDirectory>
  <phasecalc:OutputDirectory>fgip</phasecalc:OutputDirectory>
</phasecalc:RuntimeInformation>
"""
        actual_xml = self.runtime_inf.write_to_xml()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )

    def test_write_phasetransfer_jar_xml(self):
        """Test expected XML produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:PhaseTransferJar xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">resources/example.jar</phasecalc:PhaseTransferJar>
"""
        actual_xml = self.runtime_inf.write_phasetransfer_jar_xml()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )

    def test_write_scratch_dir_xml(self):
        """Test scratch directory XML is produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:ScratchDirectory xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">scratch</phasecalc:ScratchDirectory>
"""
        actual_xml = self.runtime_inf.write_scratch_dir_xml()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )

    def test_write_output_dir_xml(self):
        """Test output directory XML is produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:OutputDirectory xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">fgip</phasecalc:OutputDirectory>
"""
        actual_xml = self.runtime_inf.write_output_dir_xml()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )
