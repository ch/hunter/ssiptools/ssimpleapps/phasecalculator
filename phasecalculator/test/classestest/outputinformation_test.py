# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for output information class tests.

@author: Mark
"""

import logging
from lxml import etree
import unittest
from phasecalculator.classes.outputinformation import OutputInformation

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class OutputInformationTestCase(unittest.TestCase):
    """Test case for OutputInformation class."""

    def setUp(self):
        """Set up for tests.

        Returns
        -------
        None.

        """
        self.out_inf = OutputInformation(True, True, "all", True)

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        del self.out_inf

    def test_parse_xml(self):
        """Test expected information is parsed.

        Returns
        -------
        None.

        """
        actual_output = OutputInformation.parse_xml(self.out_inf.write_to_xml())
        self.assertEqual(self.out_inf, actual_output)

    def test_parse_fgip_output(self):
        """Test expected information is parsed.

        Returns
        -------
        None.

        """
        actual_output = OutputInformation.parse_fgip_output(self.out_inf.write_to_xml())
        self.assertTrue(actual_output)

    def test_parse_similarity_output(self):
        """Test expected information is parsed.

        Returns
        -------
        None.

        """
        actual_output, actual_type = OutputInformation.parse_similarity_output(
            self.out_inf.write_to_xml()
        )
        self.assertTrue(actual_output)
        self.assertEqual("all", actual_type)

    def test_parse_vle_output(self):
        """Test expected information is parsed.

        Returns
        -------
        None.

        """
        actual_output = OutputInformation.parse_vle_output(self.out_inf.write_to_xml())
        self.assertTrue(actual_output)

    def test_write_to_xml(self):
        """Test expected XML produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:OutputInformation xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">
  <phasecalc:FGIPOutput>true</phasecalc:FGIPOutput>
  <phasecalc:SimilarityOutput phasecalc:outputType="all">true</phasecalc:SimilarityOutput>
  <phasecalc:VLEOutput>true</phasecalc:VLEOutput>
</phasecalc:OutputInformation>
"""
        actual_xml = self.out_inf.write_to_xml()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )

    def test_write_fgip_output(self):
        """Test expected XML produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:FGIPOutput xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">true</phasecalc:FGIPOutput>
"""
        actual_xml = self.out_inf.write_fgip_output()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )

    def test_write_similarity_output(self):
        """Test expected XML produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:SimilarityOutput xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema" phasecalc:outputType="all">true</phasecalc:SimilarityOutput>
"""
        actual_xml = self.out_inf.write_similarity_output()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )

    def test_write_vle_output(self):
        """Test expected XML produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:VLEOutput xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">true</phasecalc:VLEOutput>
"""
        actual_xml = self.out_inf.write_vle_output()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )
