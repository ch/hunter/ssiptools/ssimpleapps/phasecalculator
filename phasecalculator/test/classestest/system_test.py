# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for System and SystemCollection class tests.

@author: Mark
"""

import logging
from lxml import etree
import unittest
from phasecalculator.classes.temperature import Temperature
from phasecalculator.classes.molecule import Molecule
from phasecalculator.classes.phase import Phase
from phasecalculator.classes.phases import Phases
from phasecalculator.classes.runtimeinformation import RuntimeInformation
from phasecalculator.classes.outputinformation import OutputInformation
from phasecalculator.classes.system import System, SystemCollection

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SystemTestCase(unittest.TestCase):
    """System and system collection tests."""

    def setUp(self):
        """Set up before tests.

        Returns
        -------
        None.

        """
        self.maxDiff = None
        self.temperature = Temperature(298.0, "KELVIN")
        self.molecule = Molecule(
            "water",
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml",
            1.0,
        )
        self.phase = Phase([self.molecule], self.temperature)
        self.phases = Phases([self.phase])
        self.example_jar = "resources/example.jar"
        self.runtime_inf = RuntimeInformation(self.example_jar, "scratch", "fgip")
        self.out_inf = OutputInformation(True, True, "all", True)
        self.system = System(self.phases, self.runtime_inf, self.out_inf)
        self.system_collection = SystemCollection([self.system])

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        del self.phase
        del self.molecule
        del self.temperature
        del self.phases
        del self.runtime_inf
        del self.out_inf
        del self.system
        del self.system_collection

    def test_coll_parse_xml(self):
        """Test expected system object is produced.

        Returns
        -------
        None.

        """
        actual_collection = SystemCollection.parse_xml(
            self.system_collection.write_to_xml()
        )
        self.assertEqual(self.system_collection, actual_collection)

    def test_coll_write_to_xml(self):
        """Test expected XML is produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:SystemCollection xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">
  <phasecalc:System>
    <phasecalc:Phases>
      <phasecalc:Phase>
        <phasecalc:Molecule phasecalc:name="water" phasecalc:inChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phasecalc:ssipFileLocation="XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml" phasecalc:molefraction="1.0000"/>
        <phasecalc:Temperature phasecalc:value="298.000" phasecalc:unit="KELVIN"/>
      </phasecalc:Phase>
    </phasecalc:Phases>
    <phasecalc:RuntimeInformation>
      <phasecalc:PhaseTransferJar>resources/example.jar</phasecalc:PhaseTransferJar>
      <phasecalc:ScratchDirectory>scratch</phasecalc:ScratchDirectory>
      <phasecalc:OutputDirectory>fgip</phasecalc:OutputDirectory>
    </phasecalc:RuntimeInformation>
    <phasecalc:OutputInformation>
      <phasecalc:FGIPOutput>true</phasecalc:FGIPOutput>
      <phasecalc:SimilarityOutput phasecalc:outputType="all">true</phasecalc:SimilarityOutput>
      <phasecalc:VLEOutput>true</phasecalc:VLEOutput>
    </phasecalc:OutputInformation>
  </phasecalc:System>
</phasecalc:SystemCollection>
"""
        actual_xml = self.system_collection.write_to_xml()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )

    def test_sys_parse_xml(self):
        """Test expected system object is produced.

        Returns
        -------
        None.

        """
        actual_system = System.parse_xml(self.system.write_to_xml())
        self.assertEqual(self.system, actual_system)

    def test_sys_write_to_xml(self):
        """Test expected XML is produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:System xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">
  <phasecalc:Phases>
    <phasecalc:Phase>
      <phasecalc:Molecule phasecalc:name="water" phasecalc:inChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phasecalc:ssipFileLocation="XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml" phasecalc:molefraction="1.0000"/>
      <phasecalc:Temperature phasecalc:value="298.000" phasecalc:unit="KELVIN"/>
    </phasecalc:Phase>
  </phasecalc:Phases>
  <phasecalc:RuntimeInformation>
    <phasecalc:PhaseTransferJar>resources/example.jar</phasecalc:PhaseTransferJar>
    <phasecalc:ScratchDirectory>scratch</phasecalc:ScratchDirectory>
    <phasecalc:OutputDirectory>fgip</phasecalc:OutputDirectory>
  </phasecalc:RuntimeInformation>
  <phasecalc:OutputInformation>
    <phasecalc:FGIPOutput>true</phasecalc:FGIPOutput>
    <phasecalc:SimilarityOutput phasecalc:outputType="all">true</phasecalc:SimilarityOutput>
    <phasecalc:VLEOutput>true</phasecalc:VLEOutput>
  </phasecalc:OutputInformation>
</phasecalc:System>
"""
        actual_xml = self.system.write_to_xml()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )

    def test_sys_get_phase_compositions_by_temperature(self):
        """Test expected dictionary produced.

        Returns
        -------
        None.

        """
        expected_dict = {self.temperature: [{"water": 1.0}]}
        actual_dict = self.system.get_phase_compositions_by_temperature()
        self.assertDictEqual(expected_dict, actual_dict)

    def test_get_phases_by_temp_list(self):
        """Test expected list produced.

        Returns
        -------
        None.

        """
        expected_list = [
            [
                [{"temperature_units": "KELVIN", "temperature_value": 298.0}],
                [{"water": 1.0}],
            ]
        ]
        actual_list = self.system.get_phases_by_temp_list()
        self.assertListEqual(expected_list, actual_list)

    def test_get_molefractions_by_molecule_list(self):
        """Test

        Returns
        -------
        None.

        """
        expected_list = [{"water": 1.0}]
        actual_list = self.system.get_molefractions_by_molecule_list()
        self.assertListEqual(expected_list, actual_list)

    def test_sys_get_ssip_file_locations(self):
        """Test expected set of file locations is returned.

        Returns
        -------
        None.

        """
        expected_set = {self.molecule.ssip_file_loc}
        actual_set = self.system.get_ssip_file_locations()
        self.assertSetEqual(expected_set, actual_set)

    def test_get_name_inchikey_map(self):
        """Test expected name: inchikey mapping is returned.

        Returns
        -------
        None.

        """
        expected_dict = {"water": "XLYOFNOQVPJJNP-UHFFFAOYSA-N"}
        actual_dict = self.system.get_name_inchikey_map()
        self.assertDictEqual(expected_dict, actual_dict)

    def test_calc_fgip(self):
        """Test expected bool returned.

        Returns
        -------
        None.

        """
        self.assertTrue(self.system.calc_fgip())

    def test_similarity(self):
        """Test expected bool returned.

        Returns
        -------
        None.

        """
        self.assertTrue(self.system.calc_similarity())

    def test_vle(self):
        """Test expected bool returned.

        Returns
        -------
        None.

        """
        self.assertTrue(self.system.calc_vle())
