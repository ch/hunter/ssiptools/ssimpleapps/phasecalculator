# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for phase class tests.

@author: Mark
"""

import logging
from lxml import etree
import unittest
from phasecalculator.classes.temperature import Temperature
from phasecalculator.classes.molecule import Molecule
from phasecalculator.classes.phase import Phase

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class PhaseTestCase(unittest.TestCase):
    """Test case for Phase class."""

    def setUp(self):
        """Set up before tests.

        Returns
        -------
        None.

        """
        self.temperature = Temperature(298.0, "KELVIN")
        self.molecule = Molecule(
            "water",
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml",
            1.0,
        )
        self.phase = Phase([self.molecule], self.temperature)

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        del self.phase
        del self.molecule
        del self.temperature

    def test_parse_xml(self):
        """Test expected phase object is created.

        Returns
        -------
        None.

        """
        actual_phase = Phase.parse_xml(self.phase.write_to_xml())
        self.assertEqual(self.phase, actual_phase)

    def test_parse_molecules(self):
        """Test expected molecule list is created.

        Returns
        -------
        None.

        """
        actual_mol_list = Phase.parse_molecules(self.phase.write_to_xml())
        self.assertListEqual([self.molecule], actual_mol_list)

    def test_parse_temperature(self):
        """Test expected temperature object is created.

        Returns
        -------
        None.

        """
        actual_temp = Phase.parse_temperature(self.temperature.write_to_xml())
        self.assertEqual(self.temperature, actual_temp)

    def test_write_to_xml(self):
        """Test expected XML is produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:Phase xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">
  <phasecalc:Molecule phasecalc:name="water" phasecalc:inChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phasecalc:ssipFileLocation="XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml" phasecalc:molefraction="1.0000"/>
  <phasecalc:Temperature phasecalc:value="298.000" phasecalc:unit="KELVIN"/>
</phasecalc:Phase>
"""
        actual_xml = self.phase.write_to_xml()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )

    def test_get_molefractions_by_molecule(self):
        """Test expected molefractions are returned.

        Returns
        -------
        None.

        """
        expected_dict = {"water": 1.0}
        actual_dict = self.phase.get_molefractions_by_molecule()
        self.assertDictEqual(expected_dict, actual_dict)

    def test_get_ssip_file_locations(self):
        """Test expected SSIP file locations are returned.

        Returns
        -------
        None.

        """
        expected_list = [self.molecule.ssip_file_loc]
        actual_list = self.phase.get_ssip_file_locations()
        self.assertListEqual(expected_list, actual_list)

    def test_get_name_inchikey_map(self):
        """Test expected name: inchikey mapping is returned.

        Returns
        -------
        None.

        """
        expected_dict = {"water": "XLYOFNOQVPJJNP-UHFFFAOYSA-N"}
        actual_dict = self.phase.get_name_inchikey_map()
        self.assertDictEqual(expected_dict, actual_dict)
