# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for temperature class tests.

@author: Mark
"""

import logging
from lxml import etree
import unittest
from phasecalculator.classes.temperature import Temperature

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class TemperatureTestCase(unittest.TestCase):
    """Temperature class test case."""

    def setUp(self):
        """Set up before tests.

        Returns
        -------
        None.

        """
        self.temperature = Temperature(298.0, "KELVIN")

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        del self.temperature

    def test_parse_xml(self):
        """Test expected temperature object is created.

        Returns
        -------
        None.

        """
        actual_temp = Temperature.parse_xml(self.temperature.write_to_xml())
        self.assertEqual(self.temperature, actual_temp)

    def test_write_to_xml(self):
        """Test expected XML is produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:Temperature xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema" phasecalc:value="298.000" phasecalc:unit="KELVIN"/>
"""
        actual_xml = self.temperature.write_to_xml()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )

    def test_to_dict(self):
        """Test expected dict is returned.

        Returns
        -------
        None.

        """
        expected_dict = {"temperature_value": 298.0, "temperature_units": "KELVIN"}
        actual_dict = self.temperature.to_dict()
        self.assertDictEqual(expected_dict, actual_dict)
