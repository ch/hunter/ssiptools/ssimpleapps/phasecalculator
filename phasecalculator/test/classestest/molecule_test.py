# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for temperature class tests.

@author: Mark
"""

import logging
from lxml import etree
import unittest
from phasecalculator.classes.molecule import Molecule

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class MoleculeTestCase(unittest.TestCase):
    """Test case for Molecule class."""

    def setUp(self):
        """Set up before tests.

        Returns
        -------
        None.

        """
        self.molecule = Molecule(
            "water",
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml",
            1.0,
        )

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        del self.molecule

    def test_parse_xml(self):
        """Test expected molecule object is created.

        Returns
        -------
        None.

        """
        actual_molecule = Molecule.parse_xml(self.molecule.write_to_xml())
        self.assertEqual(self.molecule, actual_molecule)

    def test_write_to_xml(self):
        """Test expected XML is produced.

        Returns
        -------
        None.

        """
        expected_xml = """<phasecalc:Molecule xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema" phasecalc:name="water" phasecalc:inChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phasecalc:ssipFileLocation="XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml" phasecalc:molefraction="1.0000"/>
"""
        actual_xml = self.molecule.write_to_xml()
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml, pretty_print=True)
        )
