# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Test case for phasexmlcreatorrunner module.

@author: mark
"""

import logging
import unittest
import pathlib
import os
import shutil
from lxml import etree
import puresolventinformation.information as pureinf
from phasecalculator.classes.temperature import Temperature
from phasecalculator.classes.molecule import Molecule
from phasecalculator.classes.phase import Phase
from phasecalculator.classes.phases import Phases
from phasecalculator.classes.runtimeinformation import RuntimeInformation
from phasecalculator.classes.outputinformation import OutputInformation
from phasecalculator.classes.system import System
import phasecalculator.runners.phasexmlcreatorrunner as phasecreaterun

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class PhaseXMLCreatorRunnerTestCase(unittest.TestCase):
    """Test case for phase XML creator runner methods."""

    def setUp(self):
        """Set up for tests.

        Returns
        -------
        None.

        """
        self.maxDiff = None
        ssip_filenames = pureinf.get_ssip_file_dict()
        self.water_ssip = ssip_filenames["XLYOFNOQVPJJNP-UHFFFAOYSA-N"]
        self.butanol_ssip = ssip_filenames["LRHPLDYGYMQRHN-UHFFFAOYSA-N"]
        self.temperature = Temperature(298.0, "KELVIN")
        self.water_molecule = Molecule(
            "water", "XLYOFNOQVPJJNP-UHFFFAOYSA-N", self.water_ssip, 0.8354716981132075
        )
        self.butanol_molecule = Molecule(
            "1-butanol",
            "LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            self.butanol_ssip,
            0.16452830188679246,
        )
        self.phase = Phase(
            [self.water_molecule, self.butanol_molecule], self.temperature
        )
        self.phases = Phases([self.phase])
        self.example_jar = "resources/example.jar"
        self.runtime_inf = RuntimeInformation(self.example_jar, "scratch", "fgip")
        self.out_inf = OutputInformation(True, True, "all", True)
        self.system = System(self.phases, self.runtime_inf, self.out_inf)
        phasecreaterun.create_scratch_dir(self.system)
        parent_directory = pathlib.Path(__file__).parents[1]
        self.expected_phase_file = (
            (parent_directory / "resources" / "expected_phase.xml")
            .absolute()
            .as_posix()
        )
        self.expected_solvent_file = (
            (parent_directory / "resources" / "expected_solvent.xml")
            .absolute()
            .as_posix()
        )

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        for filename in os.listdir(self.runtime_inf.scratch_dir):
            file_path = os.path.join(self.runtime_inf.scratch_dir, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                LOGGER.error("Failed to delete %s. Reason: %s", file_path, e)
        os.rmdir(self.runtime_inf.scratch_dir)

    def test_create_phase_and_solvent_files(self):
        """Test

        Returns
        -------
        None.

        """
        (
            phase_filename,
            solvent_filename_list,
        ) = phasecreaterun.create_phase_and_solvent_files(self.system)
        self.assertEqual("scratch/systemphase.xml", phase_filename)
        with open(phase_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phase_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)
        expected_list = [
            {
                "solvent_filename": "scratch/systemsolvent_298.0K.xml",
                "temperature_units": "KELVIN",
                "temperature_value": 298.0,
            }
        ]
        self.assertListEqual(expected_list, solvent_filename_list)
        solvent_filename = solvent_filename_list[0]["solvent_filename"]
        with open(solvent_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_solvent_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_create_phase_file(self):
        """Test

        Returns
        -------
        None.

        """
        phase_filename = phasecreaterun.create_phase_file(self.system, "systemphase")
        self.assertEqual("scratch/systemphase.xml", phase_filename)
        with open(phase_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phase_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_create_solvent_files(self):
        """Test

        Returns
        -------
        None.

        """
        expected_list = [
            {
                "solvent_filename": "scratch/systemsolvent_298.0K.xml",
                "temperature_units": "KELVIN",
                "temperature_value": 298.0,
            }
        ]
        solvent_filename_list = phasecreaterun.create_solvent_files(
            self.system, "systemsolvent"
        )
        self.assertListEqual(expected_list, solvent_filename_list)
        solvent_filename = solvent_filename_list[0]["solvent_filename"]
        with open(solvent_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_solvent_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_create_solvent_file(self):
        """Test expected solvent XML created.

        Returns
        -------
        None.

        """
        mole_fraction_dict_list = self.system.get_phase_compositions_by_temperature()[
            self.temperature
        ]
        temp_info = self.temperature
        ssip_filename_list = self.system.get_ssip_file_locations()
        solvent_filename = phasecreaterun.create_solvent_file(
            mole_fraction_dict_list,
            ssip_filename_list,
            temp_info,
            "systemsolvent",
            "scratch",
        )
        self.assertEqual("scratch/systemsolvent_298.0K.xml", solvent_filename)
        with open(solvent_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_solvent_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_create_scratch_dir(self):
        """Test scratch dir exists.

        Returns
        -------
        None.

        """
        self.assertTrue(pathlib.Path("scratch").is_dir())

    def test_create_solvent_filename(self):
        """Test expected string produced.

        Returns
        -------
        None.

        """
        expected_filename = "scratch/systemsolvent_298.0K.xml"
        actual_filename = phasecreaterun.create_solvent_filename(
            "scratch", self.temperature, "systemsolvent"
        )
        self.assertEqual(expected_filename, actual_filename)

    def test_create_phase_filename(self):
        """Test expected string produced.

        Returns
        -------
        None.

        """
        expected_filename = "scratch/systemphase.xml"
        actual_filename = phasecreaterun.create_phase_filename("scratch", "systemphase")
        self.assertEqual(expected_filename, actual_filename)

    def test_create_temperaturestring(self):
        """Test expected string produced.

        Returns
        -------
        None.

        """
        expected_string = "298.0K"
        actual_string = phasecreaterun.create_temperaturestring(298.0, "KELVIN")
        self.assertEqual(expected_string, actual_string)
