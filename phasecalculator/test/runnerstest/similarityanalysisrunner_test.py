# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for similarity analysis runner tests.

@author: Mark
"""

import logging
import unittest
import pandas
import pathlib
import os
import shutil
import numpy as np
from testfixtures import Replacer
from testfixtures.popen import MockPopen
import phasecalculator.runners.similarityanalysisrunner as simrun


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SimilarityAnalysisRunnerTestCase(unittest.TestCase):
    """Test case for similarity analysis."""

    def setUp(self):
        """Set up before tests.

        Returns
        -------
        None.

        """
        self.Popen = MockPopen()
        self.Popen.set_default()
        self.r = Replacer()
        self.r.replace(
            "phasecalculator.runners.phasetransferrunner.run_calculation", self.Popen
        )
        self.addCleanup(self.r.restore)
        parent_directory = pathlib.Path(__file__).parents[1]
        self.example_jar = (
            (parent_directory / "resources" / "example.jar").absolute().as_posix()
        )
        self.phase_filename = (
            (parent_directory / "resources" / "expected_phase.xml")
            .absolute()
            .as_posix()
        )
        self.solvent_filename = (
            (parent_directory / "resources" / "expected_solvent.xml")
            .absolute()
            .as_posix()
        )
        self.free_output_filename = (
            (parent_directory / "resources" / "expected_freeenergy.xml")
            .absolute()
            .as_posix()
        )
        self.directory = simrun.generate_directory("sim", 298.0, "KELVIN")
        self.energy_xmlfile = "sim/298_0K/1-butanol0.164528302water0.835471698free.xml"
        self.poly_file = (
            "sim/298_0K/1-butanol0.164528302water0.835471698free_poly_fit_split.csv"
        )
        (
            self.out_res,
            self.filename_list,
        ) = simrun.extract_solvents_and_generate_polynomials(
            self.solvent_filename, self.free_output_filename, self.directory
        )
        self.solv_energy_dict = {
            "solvent_filename": self.solvent_filename,
            "energy_filename": self.free_output_filename,
            "temperature_value": 298.0,
            "temperature_units": "KELVIN",
        }

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        for filename in os.listdir("sim"):
            file_path = os.path.join("sim", filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                LOGGER.error("Failed to delete %s. Reason: %s", file_path, e)
        os.rmdir("sim")

    def test_run_similarity_analysis(self):
        """Test

        Returns
        -------
        None.

        """
        simrun.run_similarity_analysis([self.poly_file], "all", "sim")
        self.assertTrue((pathlib.Path("sim") / "similaritymatrix.csv").is_file())

    def test_extract_all_solvents_and_generate_polynomials(self):
        """Test

        Returns
        -------
        None.

        """
        out_res, filename_list = simrun.extract_all_solvents_and_generate_polynomials(
            [self.solv_energy_dict], "sim"
        )
        self.assertListEqual([0], out_res)
        self.assertListEqual([self.poly_file], filename_list)

    def test_generate_directory(self):
        """Test

        Returns
        -------
        None.

        """
        self.assertTrue(pathlib.Path(self.directory).is_dir())

    def test_extract_solvents_and_generate_polynomials(self):
        """Test

        Returns
        -------
        None.

        """
        self.assertListEqual([0], self.out_res)
        self.assertListEqual([self.poly_file], self.filename_list)

    def test_run_free_energy_calculation(self):
        """Test

        Returns
        -------
        None.

        """
        expected_args = [
            "java",
            "-jar",
            self.example_jar,
            "--energies",
            "solvationtotal",
            "--calc",
            "--conccalc",
            "-o",
            self.free_output_filename,
            "--solvent",
            self.solvent_filename,
            "--solute",
            simrun.phaserun.pureinf.SINGLE_SSIP_SOLUTE_FILE,
            "--temperatureUnit",
            "KELVIN",
            "-t",
            "298.0",
        ]
        process = simrun.run_free_energy_calculation(
            self.example_jar, self.free_output_filename, self.solvent_filename
        )
        self.assertListEqual(expected_args, process.args)
