# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Test case for VLE analysis runner methods.

@author: Mark
"""

import logging
import unittest
import pathlib
import os
from testfixtures import Replacer
from testfixtures.popen import MockPopen
import phasecalculator.runners.vleanalysisrunner as vlerun

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class VLEAnalysisRunnerTestCase(unittest.TestCase):
    """Test case for VLE analysis runner methods."""

    def setUp(self):
        """Set up before tests.

        Returns
        -------
        None.

        """
        self.Popen = MockPopen()
        self.Popen.set_default()
        self.r = Replacer()
        self.r.replace(
            "phasecalculator.runners.phasetransferrunner.run_calculation", self.Popen
        )
        self.addCleanup(self.r.restore)
        self.parent_directory = pathlib.Path(__file__).parents[1]
        self.example_jar = (
            (self.parent_directory / "resources" / "example.jar").absolute().as_posix()
        )
        self.phase_filename = (
            (self.parent_directory / "resources" / "expected_phase.xml")
            .absolute()
            .as_posix()
        )
        self.phase_output_filename = (
            (self.parent_directory / "resources" / "expected_phasecalculated.xml")
            .absolute()
            .as_posix()
        )
        self.expected_phas_csv = (
            (self.parent_directory / "resources" / "expected_phasesummary.csv")
            .absolute()
            .as_posix()
        )
        self.output_file = "phase_summary.csv"

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        if os.path.isfile(self.output_file):
            os.remove(self.output_file)

    def test_calculate_and_process_vle_data(self):
        """Test expected output produced.

        Returns
        -------
        None.

        """
        vlerun.calculate_and_process_vle_data(
            self.example_jar,
            self.phase_filename,
            self.phase_output_filename,
            self.output_file,
        )
        with open(self.output_file, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phas_csv, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_process_vle_data(self):
        """Test expected output produced.

        Returns
        -------
        None.

        """
        vlerun.process_vle_data(self.phase_output_filename, self.output_file)
        with open(self.output_file, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phas_csv, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_run_vle_calculation(self):
        """Test expected system call is made.

        Returns
        -------
        None.

        """
        expected_args = [
            "java",
            "-jar",
            self.example_jar,
            "--phaseOut",
            "--phaseCollectionList",
            "--conccalc",
            "--gascalc",
            "--calc",
            "--phaseFile",
            self.phase_filename,
            "-o",
            self.phase_output_filename,
        ]
        process = vlerun.run_vle_calculation(
            self.example_jar, self.phase_filename, self.phase_output_filename
        )
        self.assertListEqual(expected_args, process.args)
