# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for phase calculator runner tests.

@author: Mark
"""

import logging
import unittest
import pathlib
import os
import shutil
import pytest
from testfixtures import Replacer
from testfixtures.popen import MockPopen

import puresolventinformation.information as pureinf
from phasecalculator.classes.temperature import Temperature
from phasecalculator.classes.molecule import Molecule
from phasecalculator.classes.phase import Phase
from phasecalculator.classes.phases import Phases
from phasecalculator.classes.runtimeinformation import RuntimeInformation
from phasecalculator.classes.outputinformation import OutputInformation
from phasecalculator.classes.system import System
import phasecalculator.runners.phasecalculatorrunner as pcalcrun


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class PhaseCalculatorRunnerTestCase(unittest.TestCase):
    """Test case for phase calculator runner methods."""

    def setUp(self):
        """Set up before tests.

        Returns
        -------
        None.

        """
        self.Popen = MockPopen()
        self.Popen.set_default()
        self.r = Replacer()
        self.r.replace(
            "phasecalculator.runners.phasetransferrunner.run_calculation", self.Popen
        )
        self.addCleanup(self.r.restore)
        ssip_filenames = pureinf.get_ssip_file_dict()
        self.water_ssip = ssip_filenames["XLYOFNOQVPJJNP-UHFFFAOYSA-N"]
        self.butanol_ssip = ssip_filenames["LRHPLDYGYMQRHN-UHFFFAOYSA-N"]
        self.temperature = Temperature(298.0, "KELVIN")
        self.water_molecule = Molecule(
            "water", "XLYOFNOQVPJJNP-UHFFFAOYSA-N", self.water_ssip, 0.8354716981132075
        )
        self.butanol_molecule = Molecule(
            "1-butanol",
            "LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            self.butanol_ssip,
            0.16452830188679246,
        )
        self.phase = Phase(
            [self.water_molecule, self.butanol_molecule], self.temperature
        )
        self.phases = Phases([self.phase])
        parent_directory = pathlib.Path(__file__).parents[1]
        self.example_jar = (
            (parent_directory / "resources/example.jar").absolute().as_posix()
        )
        self.runtime_inf = RuntimeInformation(self.example_jar, "scratch", "fgip")
        self.out_inf = OutputInformation(True, True, "all", True)
        self.system = System(self.phases, self.runtime_inf, self.out_inf)
        pcalcrun.phasexmlrun.create_scratch_dir(self.system)
        self.directory_base = pcalcrun.create_output_dir(self.system)

        self.expected_phase_file = (
            (parent_directory / "resources" / "expected_phase.xml")
            .absolute()
            .as_posix()
        )
        self.expected_solvent_file = (
            (parent_directory / "resources" / "expected_solvent.xml")
            .absolute()
            .as_posix()
        )
        self.frac_occ_filename = (
            (parent_directory / "resources/expected_solvent_fracocc.xml")
            .absolute()
            .as_posix()
        )
        self.expected_binding_file = (
            (parent_directory / "resources" / "expected_solventbinding.xml")
            .absolute()
            .as_posix()
        )
        self.expected_free_file = (
            (parent_directory / "resources" / "expected_solventfree.xml")
            .absolute()
            .as_posix()
        )
        self.expected_phase_output_filename = (
            (parent_directory / "resources" / "expected_phasecalculated.xml")
            .absolute()
            .as_posix()
        )
        self.expected_phas_csv = (
            (parent_directory / "resources" / "expected_phasesummary.csv")
            .absolute()
            .as_posix()
        )
        self.phase_filename = "scratch/systemphase.xml"
        self.phase_out_file = "scratch/systemphasecalculated.xml"
        self.output_csv_file = "fgip/systemphasecalculation_summary.csv"
        self.solv_energy_dict = {
            "solvent_filename": self.expected_solvent_file,
            "temperature_value": 298.0,
            "temperature_units": "KELVIN",
        }
        self.energy_xmlfile = "fgip/298_0K/1-butanol0.1645water0.8355binding.xml"
        self.poly_file = (
            "fgip/298_0K/1-butanol0.1645water0.8355binding_poly_fit_split.csv"
        )
        self.matrix_file = (
            "fgip/298_0K/1-butanol0.164528302water0.835471698_solv_map.csv"
        )
        self.solvent_map = (
            "fgip/298_0K/1-butanol0.164528302water0.835471698_solv_map.svg"
        )
        self.fgip_file = "fgip/298_0K/1-butanol0_164528302water0_835471698_FGIP.svg"
        self.similarity_file = "fgip/similaritymatrix.csv"
        self.expected_latex = r"""
\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\textwidth]{fgip/298_0K/1-butanol0_164528302water0_835471698_FGIP.eps}
    \caption{FGIP for 1-butanol0.164528302water0.835471698 at 298K.}
    \label{fig:1-butanol0_164528302water0_835471698solvationmap}
\end{figure}
"""

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        for filename in os.listdir(self.directory_base):
            file_path = os.path.join(self.directory_base, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                LOGGER.error("Failed to delete %s. Reason: %s", file_path, e)
        if pathlib.Path(self.directory_base).is_dir():
            os.rmdir(self.directory_base)

    def copy_intermediate_files(self):
        """Test fixture required for run all analysis test as intermediate
        energy files are not produced during test as an SSIP jar is not present.

        Returns
        -------
        None.

        """
        shutil.copyfile(
            self.expected_binding_file, "scratch/systemsolvent_298.0Kbinding.xml"
        )
        shutil.copyfile(self.expected_free_file, "scratch/systemsolvent_298.0Kfree.xml")
        shutil.copyfile(
            self.frac_occ_filename, "scratch/systemsolvent_298.0K_fracocc.xml"
        )
        shutil.copyfile(self.expected_phase_output_filename, self.phase_out_file)

    @pytest.mark.xfail
    def test_run_all_analysis(self):
        """Test

        Returns
        -------
        None.

        """
        self.copy_intermediate_files()
        pcalcrun.run_all_analysis(self.system)
        self.assertTrue(pathlib.Path(self.matrix_file).is_file())
        self.assertTrue(pathlib.Path(self.solvent_map).is_file())
        self.assertTrue(pathlib.Path(self.fgip_file).is_file())
        self.assertTrue(pathlib.Path(self.similarity_file).is_file())
        with open(self.output_csv_file, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phas_csv, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    @pytest.mark.xfail
    def test_run_fgip_analysis(self):
        """Test

        Returns
        -------
        None.

        """
        results, latex_blocks = pcalcrun.run_fgip_analysis(
            self.system, [self.solv_energy_dict]
        )
        self.assertListEqual([(0, 0)], results)
        self.assertMultiLineEqual(self.expected_latex, latex_blocks)
        self.assertTrue(pathlib.Path(self.matrix_file).is_file())
        self.assertTrue(pathlib.Path(self.solvent_map).is_file())
        self.assertTrue(pathlib.Path(self.fgip_file).is_file())

    def test_run_similarity_analysis(self):
        """Test

        Returns
        -------
        None.

        """
        pcalcrun.run_similarity_analysis(self.system, [self.solv_energy_dict])
        self.assertTrue(pathlib.Path(self.similarity_file).is_file())

    def test_run_vle_analysis(self):
        """Test

        Returns
        -------
        None.

        """
        pcalcrun.run_vle_analysis(self.system, self.phase_filename)
        with open(self.output_csv_file, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phas_csv, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_create_output_dir(self):
        """Test expected output directory created.

        Returns
        -------
        None.

        """
        self.assertTrue(
            pathlib.Path(self.system.runtime_information.output_dir).is_dir()
        )

    def test_create_phase_summary_filename(self):
        """Test create phase summary filename.

        Returns
        -------
        None.

        """
        expected_filename = "output/systemphasecalculation_summary.csv"
        actual_filename = pcalcrun.create_phase_summary_filename(
            "output", "scratch/systemphase.xml"
        )
        self.assertEqual(expected_filename, actual_filename)

    def test_create_free_energy_output_filename(self):
        """Test expected filename produced.

        Returns
        -------
        None.

        """
        expected_filename = "solvent_filenamefree.xml"
        actual_filename = pcalcrun.create_free_energy_output_filename(
            "solvent_filename.xml"
        )
        self.assertEqual(expected_filename, actual_filename)

    def test_create_binding_energy_output_filename(self):
        """Test expected filename produced.

        Returns
        -------
        None.

        """
        expected_filename = "solvent_filenamebinding.xml"
        actual_filename = pcalcrun.create_binding_energy_output_filename(
            "solvent_filename.xml"
        )
        self.assertEqual(expected_filename, actual_filename)

    def test_create_fractional_occupancy_filename(self):
        """Test expected filename produced.

        Returns
        -------
        None.

        """
        expected_filename = "solvent_filename_fracocc.xml"
        actual_filename = pcalcrun.create_fractional_occupancy_filename(
            "solvent_filename.xml"
        )
        self.assertEqual(expected_filename, actual_filename)

    def test_create_phase_output_filename(self):
        """Test expected filename produced.

        Returns
        -------
        None.

        """
        expected_filename = "phase_filenamecalculated.xml"
        actual_filename = pcalcrun.create_phase_output_filename("phase_filename.xml")
        self.assertEqual(expected_filename, actual_filename)

    def test_create_phase_and_solvent_xml_files(self):
        """Test

        Returns
        -------
        None.

        """
        (
            phase_filename,
            solvent_filename_list,
        ) = pcalcrun.create_phase_and_solvent_xml_files(self.system)
        self.assertEqual("scratch/systemphase.xml", phase_filename)
        with open(phase_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phase_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)
        expected_list = [
            {
                "solvent_filename": "scratch/systemsolvent_298.0K.xml",
                "temperature_units": "KELVIN",
                "temperature_value": 298.0,
            }
        ]
        self.assertListEqual(expected_list, solvent_filename_list)
        solvent_filename = solvent_filename_list[0]["solvent_filename"]
        with open(solvent_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_solvent_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)
