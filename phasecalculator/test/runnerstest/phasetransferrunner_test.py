# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Test case for phaetransferrunner.

@author: mark
"""

import logging
import unittest
import pathlib
from testfixtures import Replacer
from testfixtures.popen import MockPopen
import phasecalculator.runners.phasetransferrunner as phaserun

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class PhasetransferRunnerTestCase(unittest.TestCase):
    """Test case for phasetransferrunner methods."""

    def setUp(self):
        """Set up for tests.

        Returns
        -------
        None.

        """
        self.Popen = MockPopen()
        self.Popen.set_default()
        self.r = Replacer()
        self.r.replace(
            "phasecalculator.runners.phasetransferrunner.run_calculation", self.Popen
        )

        self.addCleanup(self.r.restore)
        parent_directory = pathlib.Path(__file__).parents[1]
        self.example_jar = (
            (parent_directory / "resources" / "example.jar").absolute().as_posix()
        )
        self.phase_filename = (
            (parent_directory / "resources" / "expected_phase.xml")
            .absolute()
            .as_posix()
        )
        self.solvent_filename = (
            (parent_directory / "resources" / "expected_solvent.xml")
            .absolute()
            .as_posix()
        )
        self.free_output_filename = (
            (parent_directory / "resources" / "expected_freeenergy.xml")
            .absolute()
            .as_posix()
        )
        self.binding_output_filename = (
            (parent_directory / "resources" / "expected_bindingenergy.xml")
            .absolute()
            .as_posix()
        )
        self.phase_output_filename = (
            (parent_directory / "resources" / "expected_phaseout.xml")
            .absolute()
            .as_posix()
        )
        self.phase_output_filename = (
            (parent_directory / "resources" / "expected_phaseout.xml")
            .absolute()
            .as_posix()
        )
        self.frac_occ_filename = (
            (parent_directory / "resources" / "expected_fracocc.xml")
            .absolute()
            .as_posix()
        )

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        # if pathlib.Path(self.example_jar).is_file():

    def test_run_vle_calculation(self):
        """Test subprocess called with expected arguments.

        Returns
        -------
        None.

        """
        expected_args = [
            "java",
            "-jar",
            self.example_jar,
            "--phaseOut",
            "--phaseCollectionList",
            "--conccalc",
            "--gascalc",
            "--calc",
            "--phaseFile",
            self.phase_filename,
            "-o",
            self.phase_output_filename,
        ]
        process = phaserun.run_vle_calculation(
            self.example_jar, self.phase_filename, self.phase_output_filename
        )
        self.assertListEqual(expected_args, process.args)

    def test_run_phasetransfer_binding_energy(self):
        """Test subprocess called with expected arguments.

        Returns
        -------
        None.

        """
        expected_args = [
            "java",
            "-jar",
            self.example_jar,
            "--energies",
            "solvationbinding",
            "-f",
            self.frac_occ_filename,
            "--calc",
            "--conccalc",
            "-o",
            self.binding_output_filename,
            "--solvent",
            self.solvent_filename,
            "--solute",
            phaserun.pureinf.SINGLE_SSIP_SOLUTE_FILE,
            "--temperatureUnit",
            "KELVIN",
            "-t",
            "298.0",
        ]
        process = phaserun.run_phasetransfer_binding_energy(
            self.example_jar,
            self.binding_output_filename,
            self.frac_occ_filename,
            self.solvent_filename,
        )
        self.assertListEqual(expected_args, process.args)

    def test_run_phasetransfer_free_energy(self):
        """Test subprocess called with expected arguments.

        Returns
        -------
        None.

        """
        expected_args = [
            "java",
            "-jar",
            self.example_jar,
            "--energies",
            "solvationtotal",
            "--calc",
            "--conccalc",
            "-o",
            self.free_output_filename,
            "--solvent",
            self.solvent_filename,
            "--solute",
            phaserun.pureinf.SINGLE_SSIP_SOLUTE_FILE,
            "--temperatureUnit",
            "KELVIN",
            "-t",
            "298.0",
        ]
        process = phaserun.run_phasetransfer_free_energy(
            self.example_jar, self.free_output_filename, self.solvent_filename
        )
        self.assertListEqual(expected_args, process.args)

    def test_run_calculation(self):
        """Test subprocess called with expected arguments.

        Returns
        -------
        None.

        """
        self.Popen.set_command("ls " + self.phase_filename, stdout=b"o", stderr=b"e")

        # testing of results

        process = phaserun.run_calculation(["ls", self.phase_filename])
        self.assertListEqual(["ls", self.phase_filename], process.args)

    def test_generate_vle_calc_args(self):
        """Test expected argument list produced.

        Returns
        -------
        None.

        """
        expected_args = [
            "java",
            "-jar",
            self.example_jar,
            "--phaseOut",
            "--phaseCollectionList",
            "--conccalc",
            "--gascalc",
            "--calc",
            "--phaseFile",
            self.phase_filename,
            "-o",
            self.phase_output_filename,
        ]
        actual_args = phaserun.generate_vle_calc_args(
            self.example_jar, self.phase_filename, self.phase_output_filename
        )
        self.assertListEqual(expected_args, actual_args)

    def test_generate_phasetransfer_free_args(self):
        """Test expected argument list produced.

        Returns
        -------
        None.

        """
        expected_args = [
            "java",
            "-jar",
            self.example_jar,
            "--energies",
            "solvationtotal",
            "--calc",
            "--conccalc",
            "-o",
            self.free_output_filename,
            "--solvent",
            self.solvent_filename,
            "--solute",
            phaserun.pureinf.SINGLE_SSIP_SOLUTE_FILE,
            "--temperatureUnit",
            "KELVIN",
            "-t",
            "298.0",
        ]
        actual_args = phaserun.generate_phasetransfer_free_args(
            self.example_jar, self.free_output_filename, self.solvent_filename
        )
        self.assertListEqual(expected_args, actual_args)

    def test_generate_phasetransfer_binding_args(self):
        """Test expected argument list produced.

        Returns
        -------
        None.

        """
        expected_args = [
            "java",
            "-jar",
            self.example_jar,
            "--energies",
            "solvationbinding",
            "-f",
            self.frac_occ_filename,
            "--calc",
            "--conccalc",
            "-o",
            self.binding_output_filename,
            "--solvent",
            self.solvent_filename,
            "--solute",
            phaserun.pureinf.SINGLE_SSIP_SOLUTE_FILE,
            "--temperatureUnit",
            "KELVIN",
            "-t",
            "298.0",
        ]
        actual_args = phaserun.generate_phasetransfer_binding_args(
            self.example_jar,
            self.binding_output_filename,
            self.frac_occ_filename,
            self.solvent_filename,
        )
        self.assertListEqual(expected_args, actual_args)

    def test_generate_phase_calc_args(self):
        """Test expected argument list produced.

        Returns
        -------
        None.

        """
        expected_args = [
            "--phaseOut",
            "--phaseCollectionList",
            "--conccalc",
            "--gascalc",
            "--calc",
            "--phaseFile",
            self.phase_filename,
            "-o",
            self.phase_output_filename,
        ]
        actual_args = phaserun.generate_phase_calc_args(
            self.phase_filename, self.phase_output_filename
        )
        self.assertListEqual(expected_args, actual_args)

    def test_generate_solvent_calc_args(self):
        """Test expected argument list produced.

        Returns
        -------
        None.

        """
        expected_args = [
            "--calc",
            "--conccalc",
            "-o",
            self.free_output_filename,
            "--solvent",
            self.solvent_filename,
            "--solute",
            phaserun.pureinf.SINGLE_SSIP_SOLUTE_FILE,
            "--temperatureUnit",
            "KELVIN",
            "-t",
            "298.0",
        ]
        actual_args = phaserun.generate_solvent_calc_args(
            self.free_output_filename, self.solvent_filename
        )
        self.assertListEqual(expected_args, actual_args)

    def test_make_java_arguments(self):
        """Test java call pattern matches expected pattern.

        Returns
        -------
        None.

        """
        expected_args1 = ["java", "-jar", self.example_jar]
        actual_args1 = phaserun.make_java_arguments(self.example_jar)
        self.assertListEqual(expected_args1, actual_args1)
        expected_args2 = ["java", "-Xms12000m", "-Xmx12000m", "-jar", self.example_jar]
        actual_args2 = phaserun.make_java_arguments(
            self.example_jar, memory_req="12000m"
        )
        self.assertListEqual(expected_args2, actual_args2)

    def test_check_jar_exists(self):
        """Test if file exists.

        Returns
        -------
        None.

        """
        self.assertTrue(phaserun.check_jar_exists(self.example_jar))
