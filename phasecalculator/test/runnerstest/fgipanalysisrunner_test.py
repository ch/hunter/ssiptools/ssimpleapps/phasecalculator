# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Test case for FGIP analysis runner methods.

@author: Mark
"""

import logging
import unittest
import pathlib
import os
import shutil
import pytest
from testfixtures import Replacer
from testfixtures.popen import MockPopen
import phasecalculator.runners.fgipanalysisrunner as fgiprunner

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class FGIPAnalysisRunnerTestCase(unittest.TestCase):
    """Test case for fgip analysis runner methods."""

    def setUp(self):
        """Set up before tests.

        Returns
        -------
        None.

        """
        self.maxDiff = None
        self.Popen = MockPopen()
        self.Popen.set_default()
        self.r = Replacer()
        self.r.replace(
            "phasecalculator.runners.phasetransferrunner.run_calculation", self.Popen
        )
        self.addCleanup(self.r.restore)

        self.parent_directory = pathlib.Path(__file__).parents[1]
        self.solvent_filename = (
            (self.parent_directory / "resources/expected_solvent.xml")
            .absolute()
            .as_posix()
        )
        self.frac_occ_filename = (
            (self.parent_directory / "resources/expected_solvent_fracocc.xml")
            .absolute()
            .as_posix()
        )
        self.example_jar = (
            (self.parent_directory / "resources" / "example.jar").absolute().as_posix()
        )
        self.binding_output_filename = (
            (self.parent_directory / "resources" / "expected_bindingenergy.xml")
            .absolute()
            .as_posix()
        )
        self.directory_base = "fgip"
        self.directory = fgiprunner.generate_directory(
            self.directory_base, 298.0, "KELVIN"
        )
        (
            self.out_res,
            self.filename_list,
        ) = fgiprunner.extract_solvents_and_generate_polynomials(
            self.solvent_filename, self.binding_output_filename, self.directory
        )
        self.energy_xmlfile = (
            "fgip/298_0K/1-butanol0.164528302water0.835471698binding.xml"
        )
        self.poly_file = (
            "fgip/298_0K/1-butanol0.164528302water0.835471698binding_poly_fit_split.csv"
        )
        self.matrix_file = (
            "fgip/298_0K/1-butanol0.164528302water0.835471698_solv_map.csv"
        )
        self.solvent_map = (
            "fgip/298_0K/1-butanol0.164528302water0.835471698_solv_map.svg"
        )
        self.fgip_file = "fgip/298_0K/1-butanol0_164528302water0_835471698_FGIP.svg"
        self.expected_latex = r"""
\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\textwidth]{fgip/298_0K/1-butanol0_164528302water0_835471698_FGIP.eps}
    \caption{FGIP for 1-butanol0.164528302water0.835471698 at 298K.}
    \label{fig:1-butanol0_164528302water0_835471698solvationmap}
\end{figure}
"""

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        for filename in os.listdir(self.directory_base):
            file_path = os.path.join(self.directory_base, filename)
            try:
                if os.path.isfile(file_path) or os.path.islink(file_path):
                    os.unlink(file_path)
                elif os.path.isdir(file_path):
                    shutil.rmtree(file_path)
            except Exception as e:
                LOGGER.error("Failed to delete %s. Reason: %s", file_path, e)
        if pathlib.Path(self.directory_base).is_dir():
            os.rmdir(self.directory_base)

    @pytest.mark.xfail
    def test_calc_energies_and_fgips(self):
        """Test calculation and FGIP analysis is run.

        Returns
        -------
        None.

        """
        results, latex_blocks = fgiprunner.calc_energies_and_fgips(
            self.example_jar,
            self.binding_output_filename,
            self.frac_occ_filename,
            self.solvent_filename,
            self.directory_base,
        )
        self.assertListEqual([(0, 0)], results)
        self.assertMultiLineEqual(self.expected_latex, latex_blocks)
        self.assertTrue(pathlib.Path(self.matrix_file).is_file())
        self.assertTrue(pathlib.Path(self.solvent_map).is_file())
        self.assertTrue(pathlib.Path(self.fgip_file).is_file())

    def test_generate_directory(self):
        """Test directory is generated.

        Returns
        -------
        None.

        """
        self.assertTrue(pathlib.Path(self.directory).is_dir())

    @pytest.mark.xfail
    def test_run_fgip_analysis(self):
        """Test FGIP analysis is run.

        Returns
        -------
        None.

        """
        results, latex_blocks = fgiprunner.run_fgip_analysis(
            self.frac_occ_filename, self.filename_list, self.directory
        )
        self.assertListEqual([(0, 0)], results)
        self.assertMultiLineEqual(self.expected_latex, latex_blocks)
        self.assertTrue(pathlib.Path(self.matrix_file).is_file())
        self.assertTrue(pathlib.Path(self.solvent_map).is_file())
        self.assertTrue(pathlib.Path(self.fgip_file).is_file())

    def test_extract_solvents_and_generate_polynomials(self):
        """Test expected solvent information is extracted and binding polynomial is produced.

        Returns
        -------
        None.

        """
        self.assertListEqual([0], self.out_res)
        self.assertListEqual([self.poly_file], self.filename_list)

    def test_run_binding_energy_calculation(self):
        """Test expected system call is produced.

        Returns
        -------
        None.

        """
        expected_args = [
            "java",
            "-jar",
            self.example_jar,
            "--energies",
            "solvationbinding",
            "-f",
            self.frac_occ_filename,
            "--calc",
            "--conccalc",
            "-o",
            self.binding_output_filename,
            "--solvent",
            self.solvent_filename,
            "--solute",
            fgiprunner.phaserun.pureinf.SINGLE_SSIP_SOLUTE_FILE,
            "--temperatureUnit",
            "KELVIN",
            "-t",
            "298.0",
        ]
        process = fgiprunner.run_binding_energy_calculation(
            self.example_jar,
            self.binding_output_filename,
            self.frac_occ_filename,
            self.solvent_filename,
        )
        self.assertListEqual(expected_args, process.args)
