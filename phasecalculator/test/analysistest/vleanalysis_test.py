#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
# -*- coding: utf-8 -*-
"""
Script for VLE analysis tests.

@author: Mark
"""

import logging
import unittest
import pathlib
import os
import phasecalculator.analysis.vleanalysis as vleanalysis

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class VLEAnalysisTestCase(unittest.TestCase):
    """Test case for VLE analysis methods."""

    def setUp(self):
        """Set up environment for tests.

        Returns
        -------
        None.

        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[1]
        self.calculated_phase_file = (
            (parent_directory / "resources" / "expected_phasecalculated.xml")
            .absolute()
            .as_posix()
        )
        self.expected_phas_csv = (
            (parent_directory / "resources" / "expected_phasesummary.csv")
            .absolute()
            .as_posix()
        )
        self.mixture_info_dict = vleanalysis.read_data_from_mixtures(
            self.calculated_phase_file
        )
        self.processed_list = vleanalysis.convert_mixture_container_dict_to_list(
            self.mixture_info_dict
        )
        self.output_file = "phase_summary.csv"

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        if os.path.isfile(self.output_file):
            os.remove(self.output_file)

    def test_process_vle_data(self):
        """Test expected file is produced.

        Returns
        -------
        None.

        """
        vleanalysis.process_vle_data(self.calculated_phase_file, self.output_file)
        with open(self.output_file, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phas_csv, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_write_data_to_csv(self):
        """Test Expected file is produced.

        Returns
        -------
        None.

        """
        vleanalysis.write_data_to_csv(self.processed_list, self.output_file)
        with open(self.output_file, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phas_csv, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_convert_mixture_container_dict_to_list(self):
        """Test expected list created.

        Returns
        -------
        None.

        """
        expected_list = [
            [
                "1-butanol0.165water0.835298.000KELVIN",
                298.0,
                "GAS",
                "LRHPLDYGYMQRHN-UHFFFAOYSA-N",
                "0.0000021914",
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                "0.0008190888",
            ],
            [
                "1-butanol0.165water0.835298.000KELVIN",
                298.0,
                "CONDENSED",
                "LRHPLDYGYMQRHN-UHFFFAOYSA-N",
                "4.4440094829",
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                "30.7036536932",
            ],
        ]
        actual_list = self.processed_list
        self.assertListEqual(expected_list, actual_list)

    def test_convert_phase_dict_to_list(self):
        """Test conversion of phase information.
 
        Returns
        -------
        None.

        """
        expected_list = [
            "1-butanol0.165water0.835298.000KELVIN",
            298.0,
            "GAS",
            "LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            "0.0000021914",
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "0.0008190888",
        ]
        actual_list = vleanalysis.convert_phase_dict_to_list(
            self.mixture_info_dict["1-butanol0.165water0.835298.000KELVIN"][
                "1-butanol0.165water0.835298.000KELVINGAS"
            ]
        )
        self.assertEqual(expected_list, actual_list)

    def test_read_data_from_mixtures(self):
        """Test data is read in as expected.

        Returns
        -------
        None.

        """
        expected_dict = {
            "1-butanol0.165water0.835298.000KELVIN": {
                "1-butanol0.165water0.835298.000KELVINGAS": {
                    "phase_type": "GAS",
                    "temperature_value": 298.0,
                    "temperature_unit": "KELVIN",
                    "solvent_id": "1-butanol0.165water0.835298.000KELVIN",
                    "concentrations": {
                        "LRHPLDYGYMQRHN-UHFFFAOYSA-N": {
                            "concentration_value": 2.1914464984900333e-06,
                            "concentration_unit": "MOLAR",
                        },
                        "XLYOFNOQVPJJNP-UHFFFAOYSA-N": {
                            "concentration_value": 0.0008190888071552708,
                            "concentration_unit": "MOLAR",
                        },
                    },
                },
                "1-butanol0.165water0.835298.000KELVINCONDENSED": {
                    "phase_type": "CONDENSED",
                    "temperature_value": 298.0,
                    "temperature_unit": "KELVIN",
                    "solvent_id": "1-butanol0.165water0.835298.000KELVIN",
                    "concentrations": {
                        "LRHPLDYGYMQRHN-UHFFFAOYSA-N": {
                            "concentration_value": 4.444009482942443,
                            "concentration_unit": "MOLAR",
                        },
                        "XLYOFNOQVPJJNP-UHFFFAOYSA-N": {
                            "concentration_value": 30.70365369323062,
                            "concentration_unit": "MOLAR",
                        },
                    },
                },
            }
        }
        actual_dict = self.mixture_info_dict
        LOGGER.debug(actual_dict)
        self.assertDictEqual(expected_dict, actual_dict)
