# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for similarity analysis tests.

@author: Mark
"""

import logging
import unittest
import pandas
import pathlib
import numpy as np
import os
import phasecalculator.analysis.similarityanalysis as simanalysis


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SimilarityAnalysisTestCase(unittest.TestCase):
    """Test case for similarity analysis."""

    def setUp(self):
        """Set up before tests.

        Returns
        -------
        None.

        """
        parent_directory = pathlib.Path(__file__).parents[1]
        self.expected_free_poly = (
            (parent_directory / "resources" / "expected_parsedfreepoly.csv")
            .absolute()
            .as_posix()
        )
        self.expected_matrix_file = (
            (parent_directory / "resources" / "expectd_matrix.csv")
            .absolute()
            .as_posix()
        )
        self.example_poly_data = simanalysis.read_polynomial_files(
            [self.expected_free_poly], suffix="freepoly.csv", temperature_dir=True
        )
        self.example_with_pure_data = {
            **simanalysis.PURE_SOLVENT_INFO,
            **self.example_poly_data,
        }
        self.example_matrix = simanalysis.calculate_similarity_matrix(
            self.example_with_pure_data
        )
        self.actual_matrix_file = "actual_matrix.csv"
        self.actual_series_file = "resourcesexpected_parsedfreepoly.csv.csv"

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        del self.expected_free_poly
        del self.expected_matrix_file
        del self.example_with_pure_data
        del self.example_matrix
        if os.path.isfile(self.actual_matrix_file):
            os.remove(self.actual_matrix_file)
        if os.path.isfile(self.actual_series_file):
            os.remove(self.actual_series_file)
        del self.actual_matrix_file
        del self.actual_series_file

    def test_read_polynomial_files(self):
        """Test expected information is read.

        Returns
        -------
        None.

        """
        expected_dict = {
            "resourcesexpected,parsed": {
                8: {
                    "negative": {
                        "RMSE": 0.0137444779,
                        "coefficients": np.array(
                            [
                                -2.60836624e-01,
                                4.65295250e-01,
                                2.57637417e-01,
                                2.19491128e-01,
                                4.20087475e-02,
                                4.02289353e-03,
                                2.12247938e-04,
                                5.89243946e-06,
                                6.73443996e-08,
                            ]
                        ),
                        "covar": 0.0379710451935,
                        "order": 8,
                    },
                    "positive": {
                        "RMSE": 0.008907316,
                        "coefficients": np.array(
                            [
                                -2.52075337e-01,
                                -7.07009850e-01,
                                9.86359325e-01,
                                -1.40859321e00,
                                5.04060101e-01,
                                -9.17970326e-02,
                                9.28812463e-03,
                                -4.97458825e-04,
                                1.10198937e-05,
                            ]
                        ),
                        "covar": 0.008013368123414,
                        "order": 8,
                    },
                }
            }
        }
        actual_dict = self.example_poly_data
        self.assertListEqual(list(expected_dict.keys()), list(actual_dict.keys()))
        for region, info_dict in actual_dict["resourcesexpected,parsed"][8].items():
            for key, value in info_dict.items():
                if key == "coefficients":
                    np.testing.assert_allclose(
                        value, expected_dict["resourcesexpected,parsed"][8][region][key]
                    )
                else:
                    self.assertEqual(
                        value, expected_dict["resourcesexpected,parsed"][8][region][key]
                    )

    def test_compare_all_solvents(self):
        """Test complete matrix is produced.

        Returns
        -------
        None.

        """
        simanalysis.compare_all_solvents(
            [self.expected_free_poly], "", self.actual_matrix_file
        )
        self.assertTrue(pathlib.Path(self.actual_matrix_file).is_file())
        actual_dataframe = pandas.read_csv(
            self.actual_matrix_file, sep="\t", header=0, index_col=0
        )
        self.assertEqual((262, 262), actual_dataframe.shape)

    def test_compare_with_pure_solvents(self):
        """Test column matrix is produced.

        Returns
        -------
        None.

        """
        simanalysis.compare_with_pure_solvents([self.expected_free_poly], "")
        self.assertTrue(pathlib.Path(self.actual_series_file).is_file())
        actual_dataframe = pandas.read_csv(
            self.actual_series_file, sep="\t", header=0, index_col=0
        )
        self.assertEqual((261, 1), actual_dataframe.shape)

    def test_compare_solvent_list(self):
        """Test expected matrix is produced.

        Returns
        -------
        None.

        """
        simanalysis.compare_solvent_list(
            [self.expected_free_poly], "", self.actual_matrix_file
        )
        self.assertTrue(pathlib.Path(self.actual_matrix_file).is_file())
        actual_dataframe = pandas.read_csv(
            self.actual_matrix_file, sep="\t", header=0, index_col=0
        )
        self.assertEqual((1, 1), actual_dataframe.shape)

    def test_write_frame_to_file(self):
        """Test expected file is exists.

        Returns
        -------
        None.

        """
        simanalysis.write_frame_to_file(self.example_matrix, self.actual_matrix_file)
        self.assertTrue(pathlib.Path(self.actual_matrix_file).is_file())

    def test_extract_comparison_to_pure_solvents(self):
        """Test expected comparisons are produced.

        Returns
        -------
        None.

        """
        extracted_data = simanalysis.extract_comparison_to_pure_solvents(
            self.example_matrix, "resourcesexpected,parsed"
        )
        expected_top5 = pandas.Series(
            data={
                "formamide": 0.07340402,
                "hydrogen peroxide": 0.12358179,
                "methanesulfonic acid": 0.12495022,
                "ethylene carbonate": 0.14666921,
                "acetic acid": 0.15362136,
            },
            name="resourcesexpected,parsed",
        )
        np.testing.assert_allclose(expected_top5.values, extracted_data[:5].values)
        self.assertListEqual(
            expected_top5.index.to_list(), extracted_data[:5].index.to_list()
        )

    def test_calculate_similarity_matrix(self):
        """Test expected dataframe produced.

        Returns
        -------
        None.

        """
        actual_dataframe = self.example_matrix
        self.assertEqual((262, 262), actual_dataframe.shape)
