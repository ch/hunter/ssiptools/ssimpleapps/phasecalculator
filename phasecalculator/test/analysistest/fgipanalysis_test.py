# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script with test case for FGIP analysis methods.

@author: Mark
"""

import logging
import unittest
import pathlib
import os
import pytest
from testfixtures import Replacer
from testfixtures.popen import MockPopen
import phasecalculator.analysis.fgipanalysis as fgipanalysis

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class FGIPAnalysisTestCase(unittest.TestCase):
    """Test case for FGIP analysis methods."""

    def setUp(self):
        """Set up for tests.

        Returns
        -------
        None.

        """
        self.Popen = MockPopen()
        self.Popen.set_default()
        self.r = Replacer()
        self.r.replace(
            "phasecalculator.runners.phasetransferrunner.run_calculation", self.Popen
        )
        self.addCleanup(self.r.restore)
        self.maxDiff = None
        self.parent_directory = pathlib.Path(__file__).parents[1]
        self.solvent_filename = (
            (self.parent_directory / "resources/expected_solvent.xml")
            .absolute()
            .as_posix()
        )
        self.frac_occ_filename = (
            (self.parent_directory / "resources/expected_solvent_fracocc.xml")
            .absolute()
            .as_posix()
        )
        self.poly_file = (
            (
                self.parent_directory
                / "resources"
                / "testfiles"
                / "1-butanol0.164528302water0.835471698binding_poly_fit_split.csv"
            )
            .absolute()
            .as_posix()
        )
        self.matrix_file = "1-butanol0.164528302water0.835471698_solv_map.csv"
        self.solvent_map = "1-butanol0.164528302water0.835471698_solv_map.svg"
        self.fgip_file = "1-butanol0_164528302water0_835471698_FGIP.svg"
        self.expected_latex = r"""
\begin{figure}[H]
    \centering
    \includegraphics[width=0.95\textwidth]{1-butanol0_164528302water0_835471698_FGIP.eps}
    \caption{FGIP for 1-butanol0.164528302water0.835471698 at 298K.}
    \label{fig:1-butanol0_164528302water0_835471698solvationmap}
\end{figure}
"""
        self.frac_occ_inf_dict = fgipanalysis.solvmapgen.fractionaloccupancycalculator.read_fractional_occupancy_information(
            self.frac_occ_filename
        )

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        if pathlib.Path(self.matrix_file).exists():
            os.remove(self.matrix_file)
        if pathlib.Path(self.solvent_map).exists():
            os.remove(self.solvent_map)
        if pathlib.Path(self.fgip_file).is_file():
            os.remove(self.fgip_file)

    @pytest.mark.xfail
    def test_create_solv_map_and_fgips_from_files(self):
        """Test solvent map, FGIP and matrix are produced.

        Returns
        -------
        None.

        """
        results, latex_blocks = fgipanalysis.create_solv_map_and_fgips_from_files(
            self.frac_occ_filename, [self.poly_file], ""
        )
        self.assertListEqual([(0, 0)], results)
        self.assertMultiLineEqual(self.expected_latex, latex_blocks)
        self.assertTrue(pathlib.Path(self.matrix_file).is_file())
        self.assertTrue(pathlib.Path(self.solvent_map).is_file())
        self.assertTrue(pathlib.Path(self.fgip_file).is_file())

    @pytest.mark.xfail
    def test_create_fgips_from_file_list(self):
        """Test FGIPs are created.

        Returns
        -------
        None.

        """
        plot_outputs, filenames = fgipanalysis.create_solvation_plots_from_poly_files(
            fgipanalysis.EPSILON_I_LIST,
            fgipanalysis.EPSILON_J_LIST,
            self.frac_occ_filename,
            [self.poly_file],
            8,
        )
        latex_blocks = fgipanalysis.create_fgips_from_file_list(filenames)
        self.assertMultiLineEqual(self.expected_latex, latex_blocks)
        self.assertTrue(pathlib.Path(self.fgip_file).is_file())

    @pytest.mark.xfail
    def test_create_solvation_plots_from_poly_files(self):
        """Test expected solvation map and matrix file are created.

        Returns
        -------
        None.

        """
        plot_outputs, filenames = fgipanalysis.create_solvation_plots_from_poly_files(
            fgipanalysis.EPSILON_I_LIST,
            fgipanalysis.EPSILON_J_LIST,
            self.frac_occ_filename,
            [self.poly_file],
            8,
        )
        self.assertListEqual([(0, 0)], plot_outputs)
        self.assertEqual(
            ["1-butanol0.164528302water0.835471698_solv_map.svg"], filenames
        )
        self.assertTrue(pathlib.Path(self.matrix_file).is_file())
        self.assertTrue(pathlib.Path(self.solvent_map).is_file())

    def test_get_solvent_id_from_poly_filename(self):
        """Test expected solvent ID is extracted.

        Returns
        -------
        None.

        """
        self.assertEqual(
            "1-butanol0.164528302water0.835471698",
            fgipanalysis.get_solvent_id_from_poly_filename(self.poly_file),
        )

    @pytest.mark.xfail
    def test_create_solvation_plot(self):
        """Test solvent map and matrix created.

        Returns
        -------
        None.

        """
        solv_plot, matrix_res = fgipanalysis.create_solvation_plot(
            fgipanalysis.EPSILON_I_LIST,
            fgipanalysis.EPSILON_J_LIST,
            self.frac_occ_inf_dict,
            "1-butanol0.164528302water0.835471698",
            self.poly_file,
            8,
        )
        self.assertEqual(0, matrix_res)
        self.assertEqual(0, solv_plot)
        self.assertTrue(pathlib.Path(self.matrix_file).exists())
        self.assertTrue(pathlib.Path(self.solvent_map).exists())
