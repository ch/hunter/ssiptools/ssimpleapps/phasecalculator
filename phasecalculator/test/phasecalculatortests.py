# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Test runner using unittest framework.

This can also be used to run tests on the installed instance of the library.

@author: Mark
"""

import logging
import unittest
import sys
from phasecalculator.test.iotest.phasetransferxmlcreator_test import (
    PhasetransferXMLCreatorTestCase,
)
from phasecalculator.test.iotest.solventextractor_test import SolventExtractorTestCase
from phasecalculator.test.runnerstest.phasetransferrunner_test import (
    PhasetransferRunnerTestCase,
)
from phasecalculator.test.runnerstest.fgipanalysisrunner_test import (
    FGIPAnalysisRunnerTestCase,
)
from phasecalculator.test.runnerstest.vleanalysisrunner_test import (
    VLEAnalysisRunnerTestCase,
)
from phasecalculator.test.analysistest.fgipanalysis_test import FGIPAnalysisTestCase
from phasecalculator.test.classestest.molecule_test import MoleculeTestCase
from phasecalculator.test.classestest.outputinformation_test import (
    OutputInformationTestCase,
)
from phasecalculator.test.classestest.phases_test import PhasesTestCase
from phasecalculator.test.classestest.phase_test import PhaseTestCase
from phasecalculator.test.classestest.runtimeinformation_test import (
    RuntimeInformationTestCase,
)
from phasecalculator.test.classestest.system_test import SystemTestCase
from phasecalculator.test.classestest.temperature_test import TemperatureTestCase

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

IO_TEST_CASES = [PhasetransferXMLCreatorTestCase, SolventExtractorTestCase]

RUNNERS_TEST_CASES = [
    PhasetransferRunnerTestCase,
    FGIPAnalysisRunnerTestCase,
    VLEAnalysisRunnerTestCase,
]

ANALYSIS_TEST_CASES = [FGIPAnalysisTestCase]

MODULE_TEST_CASES = []

CLASSES_TEST_CASES = [
    MoleculeTestCase,
    OutputInformationTestCase,
    PhasesTestCase,
    PhaseTestCase,
    RuntimeInformationTestCase,
    SystemTestCase,
    TemperatureTestCase,
]


def create_test_suite():
    """Create a test suite with all the tests from the package.

    Returns
    -------
    suite : test suite
        test suite with all tests of package loaded.

    """
    LOGGER.info("setting up loader and test suite")
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    for test_case in IO_TEST_CASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    for test_case in RUNNERS_TEST_CASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    for test_case in ANALYSIS_TEST_CASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    for test_case in MODULE_TEST_CASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    for test_case in CLASSES_TEST_CASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    return suite


def run_tests():
    """Run test suite. Exits if there is a failure.

    Returns
    -------
    None
    """
    LOGGER.info("calling test suite method")
    suite = create_test_suite()
    LOGGER.info("running test suite")
    ret = (
        not unittest.TextTestRunner(verbosity=2, stream=sys.stderr)
        .run(suite)
        .wasSuccessful()
    )
    if ret:
        sys.exit(ret)


if __name__ == "__main__":
    run_tests()
