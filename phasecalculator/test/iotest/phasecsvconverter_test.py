# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for phases csv converter tests.

@author: Mark
"""

import logging
import unittest
import pathlib
from phasecalculator.classes.temperature import Temperature
from phasecalculator.classes.molecule import Molecule
from phasecalculator.classes.phase import Phase
from phasecalculator.classes.phases import Phases
import phasecalculator.io.phasecsvconverter as phasecsv

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class PhaseCSVConverterTestCase(unittest.TestCase):
    """Test case for phasecsvconverter methods."""

    def setUp(self):
        """Set up for tests.

        Returns
        -------
        None.

        """
        parent_directory = pathlib.Path(__file__).parents[1]
        self.example_csv = (
            (parent_directory / "resources/examplephasecomp.csv").absolute().as_posix()
        )
        self.csv_file_contents = phasecsv.read_csv_file(self.example_csv)
        self.temperature = Temperature(298.0, "KELVIN")
        self.molecule = Molecule(
            "water",
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            phasecsv.INCHIKEY_POLARISED_SSIP_FILE_DICT["XLYOFNOQVPJJNP-UHFFFAOYSA-N"],
            1.0,
        )
        self.phase = Phase([self.molecule], self.temperature)
        self.phases = Phases([self.phase])

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """

    def test_convert_csv_file_to_phases(self):
        """Test

        Returns
        -------
        None.

        """
        actual_phases = phasecsv.convert_csv_file_to_phases(self.example_csv, False)
        self.assertEqual(self.phases, actual_phases)

    def test_read_csv_file(self):
        """Test

        Returns
        -------
        None.

        """
        expected_list = [["298.0", "water", "1.0"]]
        self.assertListEqual(expected_list, self.csv_file_contents)

    def test_create_phases(self):
        """Test

        Returns
        -------
        None.

        """
        actual_phases = phasecsv.create_phases(self.csv_file_contents, False)
        self.assertEqual(self.phases, actual_phases)

    def test_create_phase(self):
        """Test

        Returns
        -------
        None.

        """
        actual_phase = phasecsv.create_phase(["298.0", "water", "1.0"], False)
        self.assertEqual(self.phase, actual_phase)

    def test_create_molecule(self):
        """Test

        Returns
        -------
        None.

        """
        actual_molecule = phasecsv.create_molecule("water", 1.0, False)
        self.assertEqual(self.molecule, actual_molecule)

    def test_create_temperature(self):
        """Test

        Returns
        -------
        None.

        """
        actual_temperature = phasecsv.create_temperature(298.0)
        self.assertEqual(self.temperature, actual_temperature)
