# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Test case for polynomialio.

@author: Mark
"""

import logging
import unittest
import pathlib
import os
import csv
import pandas
import numpy as np
import phasecalculator.io.polynomialio as polyio

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class PolynomialIOTestCase(unittest.TestCase):
    """Test case for polynomialio methods."""

    def setUp(self):
        """Set up for tests.

        Returns
        -------
        None.

        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[1]
        self.binding_file = (
            (parent_directory / "resources" / "expected_parsedbinding.xml")
            .absolute()
            .as_posix()
        )
        self.expected_binding_poly = (
            (parent_directory / "resources" / "expected_bindingpoly.csv")
            .absolute()
            .as_posix()
        )
        self.actual_binding_poly_file = (
            (parent_directory / "resources/expected_parsedbinding_poly_fit_split.csv")
            .absolute()
            .as_posix()
        )
        self.actual_free_poly_file = (
            (parent_directory / "resources" / "expected_parsedfree_poly_fit_split.csv")
            .absolute()
            .as_posix()
        )
        self.free_file = (
            (parent_directory / "resources" / "expected_parsedfree.xml")
            .absolute()
            .as_posix()
        )
        self.expected_free_poly = (
            (parent_directory / "resources" / "expected_parsedfreepoly.csv")
            .absolute()
            .as_posix()
        )

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        if os.path.isfile(self.actual_binding_poly_file):
            os.remove(self.actual_binding_poly_file)
        if os.path.isfile(self.actual_free_poly_file):
            os.remove(self.actual_free_poly_file)

    def test_read_poly_data_to_dict(self):
        """Test expected_polynomial information read in.

        Returns
        -------
        None.

        """
        expected_dict = {
            "resourcesexpected,parsed": {
                8: {
                    "negative": {
                        "RMSE": 0.0137444779,
                        "coefficients": np.array(
                            [
                                -2.60836624e-01,
                                4.65295250e-01,
                                2.57637417e-01,
                                2.19491128e-01,
                                4.20087475e-02,
                                4.02289353e-03,
                                2.12247938e-04,
                                5.89243946e-06,
                                6.73443996e-08,
                            ]
                        ),
                        "covar": 0.0379710451935,
                        "order": 8,
                    },
                    "positive": {
                        "RMSE": 0.008907316,
                        "coefficients": np.array(
                            [
                                -2.52075337e-01,
                                -7.07009850e-01,
                                9.86359325e-01,
                                -1.40859321e00,
                                5.04060101e-01,
                                -9.17970326e-02,
                                9.28812463e-03,
                                -4.97458825e-04,
                                1.10198937e-05,
                            ]
                        ),
                        "covar": 0.008013368123414,
                        "order": 8,
                    },
                }
            }
        }
        actual_dict = polyio.read_poly_data_to_dict(
            [self.expected_free_poly], suffix="freepoly.csv", temperature_dir=True
        )
        self.assertListEqual(list(expected_dict.keys()), list(actual_dict.keys()))
        for region, info_dict in actual_dict["resourcesexpected,parsed"][8].items():
            for key, value in info_dict.items():
                if key == "coefficients":
                    np.testing.assert_allclose(
                        value, expected_dict["resourcesexpected,parsed"][8][region][key]
                    )
                else:
                    self.assertEqual(
                        value, expected_dict["resourcesexpected,parsed"][8][region][key]
                    )

    def test_generate_polynomial_data_free_energy_file_list(self):
        """Test expected polynomial data outputed.

        Returns
        -------
        None.

        """
        results, filenames = polyio.generate_polynomial_data_free_energy_file_list(
            [self.free_file]
        )
        self.assertListEqual([0], results)

        with open(self.expected_free_poly, "r") as exp_poly:
            exp_contents = exp_poly.readlines()
            with open(self.actual_free_poly_file) as act_poly:
                actual_contents = act_poly.readlines()
                self.assertEqual(len(exp_contents), len(actual_contents))
                for i in range(len(exp_contents)):
                    self.assertEqual(
                        len(exp_contents[i].split("\t")),
                        len(actual_contents[i].split("\t")),
                    )

    def test_generate_polynomial_data_binding_energy_file_list(self):
        """Test expected polynomial data outputed.

        Returns
        -------
        None.

        """
        results, filenames = polyio.generate_polynomial_data_binding_energy_file_list(
            [self.binding_file]
        )
        self.assertListEqual([0], results)
        with open(self.expected_binding_poly, "r") as exp_poly:
            exp_contents = exp_poly.readlines()
            with open(self.actual_binding_poly_file) as act_poly:
                actual_contents = act_poly.readlines()
                self.assertEqual(len(exp_contents), len(actual_contents))
                for i in range(len(exp_contents)):
                    self.assertEqual(
                        len(exp_contents[i].split("\t")),
                        len(actual_contents[i].split("\t")),
                    )

    def test_generate_polynomial_filename(self):
        """Test expected filename is produced.

        Returns
        -------
        None.

        """
        actual_filename = polyio.generate_polynomial_filename("freefile.xml")
        self.assertEqual("freefile_poly_fit.csv", actual_filename)
