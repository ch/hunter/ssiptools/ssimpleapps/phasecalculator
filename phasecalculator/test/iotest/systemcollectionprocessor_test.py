# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Test case for System Collection Processor methods.

@author: Mark
"""

import logging
import unittest
from lxml import etree
import os
from phasecalculator.classes.temperature import Temperature
from phasecalculator.classes.molecule import Molecule
from phasecalculator.classes.phase import Phase
from phasecalculator.classes.phases import Phases
from phasecalculator.classes.runtimeinformation import RuntimeInformation
from phasecalculator.classes.outputinformation import OutputInformation
from phasecalculator.classes.system import System, SystemCollection
import phasecalculator.io.systemcollectionprocessor as sysproc


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SystemCollectionProcessorTestCase(unittest.TestCase):
    """Test case for  System Collection Processor methods."""

    def setUp(self):
        """Set up before tests.

        Returns
        -------
        None.

        """
        self.maxDiff = None
        self.temperature = Temperature(298.0, "KELVIN")
        self.molecule = Molecule(
            "water",
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml",
            1.0,
        )
        self.phase = Phase([self.molecule], self.temperature)
        self.phases = Phases([self.phase])
        self.example_jar = "resources/example.jar"
        self.runtime_inf = RuntimeInformation(self.example_jar, "scratch", "fgip")
        self.out_inf = OutputInformation(True, True, "all", True)
        self.system = System(self.phases, self.runtime_inf, self.out_inf)
        self.system_collection = SystemCollection([self.system])
        self.xml_filename = "syscollection.xml"
        sysproc.write_system_collection_file(self.system_collection, self.xml_filename)

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        del self.phase
        del self.molecule
        del self.temperature
        del self.phases
        del self.runtime_inf
        del self.out_inf
        del self.system
        del self.system_collection
        if os.path.isfile(self.xml_filename):
            os.remove(self.xml_filename)
        del self.xml_filename

    def test_read_system_collection_file(self):
        """Test expected collection is read in

        Returns
        -------
        None.

        """
        actual_sys_collection = sysproc.read_system_collection_file(self.xml_filename)
        self.assertEqual(self.system_collection, actual_sys_collection)

    def test_write_system_collection_file(self):
        """Test file contents produced as expected.

        Returns
        -------
        None.

        """
        expected_contents = """<?xml version='1.0' encoding='UTF-8'?>
<phasecalc:SystemCollection xmlns:phasecalc="http://www-hunter.ch.cam.ac.uk/PhaseCalculatorSchema">
  <phasecalc:System>
    <phasecalc:Phases>
      <phasecalc:Phase>
        <phasecalc:Molecule phasecalc:name="water" phasecalc:inChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phasecalc:ssipFileLocation="XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml" phasecalc:molefraction="1.0000"/>
        <phasecalc:Temperature phasecalc:value="298.000" phasecalc:unit="KELVIN"/>
      </phasecalc:Phase>
    </phasecalc:Phases>
    <phasecalc:RuntimeInformation>
      <phasecalc:PhaseTransferJar>resources/example.jar</phasecalc:PhaseTransferJar>
      <phasecalc:ScratchDirectory>scratch</phasecalc:ScratchDirectory>
      <phasecalc:OutputDirectory>fgip</phasecalc:OutputDirectory>
    </phasecalc:RuntimeInformation>
    <phasecalc:OutputInformation>
      <phasecalc:FGIPOutput>true</phasecalc:FGIPOutput>
      <phasecalc:SimilarityOutput phasecalc:outputType="all">true</phasecalc:SimilarityOutput>
      <phasecalc:VLEOutput>true</phasecalc:VLEOutput>
    </phasecalc:OutputInformation>
  </phasecalc:System>
</phasecalc:SystemCollection>
"""
        with open(self.xml_filename, "r") as actual_file:
            actual_contents = actual_file.read()
            self.assertMultiLineEqual(expected_contents, actual_contents)
