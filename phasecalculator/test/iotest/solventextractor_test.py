# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Test case for solvent extractor method.

@author: Mark
"""

import logging
import unittest
import pathlib
import os
import phasecalculator.io.solventextractor as solvextract

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SolventExtractorTestCase(unittest.TestCase):
    """Test case for solvent extractor methods."""

    def setUp(self):
        """Set up for tests.

        Returns
        -------
        None.

        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[1]
        self.binding_energy_filename = (
            (parent_directory / "resources/expected_bindingenergy.xml")
            .absolute()
            .as_posix()
        )
        self.expected_output_filename = (
            (parent_directory / "resources/expected_parsedbinding.xml")
            .absolute()
            .as_posix()
        )
        self.output_filename = "1-butanol0.164528302water0.835471698binding.xml"
        self.solvent_filename = (
            (parent_directory / "resources/expected_solvent.xml").absolute().as_posix()
        )

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        if os.path.isfile(self.output_filename):
            os.remove(self.output_filename)
        del self.output_filename

    def test_extract_and_write_energy_values_from_file(self):
        """Test extraction and writing of individual solvent energy values file.

        Returns
        -------
        None.

        """
        filenames = solvextract.extract_and_write_energy_values_from_files(
            self.solvent_filename, self.binding_energy_filename, "binding", ""
        )
        self.assertListEqual([self.output_filename], filenames)
        with open(self.output_filename, "r") as actual_file:
            actual_xml = actual_file.read()
            with open(self.expected_output_filename) as expected_file:
                expected_xml = expected_file.read()
                self.assertMultiLineEqual(expected_xml, actual_xml)
