# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Test case for phasetransferxmlcreator module.

@author: mark
"""

import logging
import unittest
import pathlib
import os
from lxml import etree
import phasecalculator.io.phasetransferxmlcreator as phasexmlmaker

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class PhasetransferXMLCreatorTestCase(unittest.TestCase):
    """Test case for phase transfer XML creator methods."""

    def setUp(self):
        """Set up for tests.

        Returns
        -------
        None.

        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[1]
        self.expected_water_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="6.0.0" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:formalCharge="0" cml:x3="-0.197781963496" cml:y3="-7.5088692891E-35" cml:z3="0.34665983175"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:formalCharge="0" cml:x3="0.760536914227" cml:y3="-9.28739754019E-37" cml:z3="0.204548524786"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:formalCharge="0" cml:x3="-0.56175495073" cml:y3="5.1742035555E-35" cml:z3="-0.551208356536"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:Surfaces>
            <ssip:Surface>
                <ssip:TotalSurfaceArea ssip:unit="Å^2">37.46449353147367</ssip:TotalSurfaceArea>
                <ssip:NegativeSurfaceArea ssip:unit="Å^2">18.417074471275452</ssip:NegativeSurfaceArea>
                <ssip:PositiveSurfaceArea ssip:unit="Å^2">19.047419060197754</ssip:PositiveSurfaceArea>
                <ssip:ElectronDensityIsosurface ssip:unit="e bohr^-3">0.002</ssip:ElectronDensityIsosurface>
                <ssip:NumberOFMEPSPoints>2267</ssip:NumberOFMEPSPoints>
                <ssip:VdWVolume ssip:unit="Å^3">21.118225673870302</ssip:VdWVolume>
                <ssip:ElectrostaticPotentialMax ssip:unit="kJ mol^-1">222.31155482388002</ssip:ElectrostaticPotentialMax>
                <ssip:ElectrostaticPotentialMin ssip:unit="kJ mol^-1">-185.78297861082</ssip:ElectrostaticPotentialMin>
            </ssip:Surface>
        </ssip:Surfaces>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.8" ssip:nearestAtomID="a2" cml:x3="1.7703677783019898" cml:y3="-0.037716579245226" cml:z3="-0.12404973071057998"/>
        <ssip:SSIP ssip:value="2.8" ssip:nearestAtomID="a3" cml:x3="-0.78007076629588" cml:y3="-0.131868324564555" cml:z3="-1.585252051411308"/>
        <ssip:SSIP ssip:value="-4.5" ssip:nearestAtomID="a1" cml:x3="-0.78007076629588" cml:y3="-1.185660785500926" cml:z3="1.34163366293468"/>
        <ssip:SSIP ssip:value="-4.5" ssip:nearestAtomID="a1" cml:x3="-0.9035034763341259" cml:y3="0.8351649972210169" cml:z3="1.5735514132586688"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.expected_butanol_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="6.0.0" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="LRHPLDYGYMQRHN-UHFFFAOYSA-N" cml:id="LRHPLDYGYMQRHN-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="C" cml:id="a1" cml:formalCharge="0" cml:x3="0.453161473082" cml:y3="-1.03446801266" cml:z3="-0.0905306598548"/>
            <cml:atom cml:elementType="H" cml:id="a10" cml:formalCharge="0" cml:x3="-0.711314642651" cml:y3="-1.77936986861" cml:z3="1.57182493824"/>
            <cml:atom cml:elementType="H" cml:id="a11" cml:formalCharge="0" cml:x3="-2.35918900768" cml:y3="-0.976902587046" cml:z3="-0.14772459368"/>
            <cml:atom cml:elementType="H" cml:id="a12" cml:formalCharge="0" cml:x3="-1.77474726333" cml:y3="-2.19616982792" cml:z3="-1.27745306344"/>
            <cml:atom cml:elementType="H" cml:id="a13" cml:formalCharge="0" cml:x3="-2.519145554" cml:y3="-4.00559237167" cml:z3="0.302707946329"/>
            <cml:atom cml:elementType="H" cml:id="a14" cml:formalCharge="0" cml:x3="-3.10209800172" cml:y3="-2.77368664521" cml:z3="1.42866821719"/>
            <cml:atom cml:elementType="H" cml:id="a15" cml:formalCharge="0" cml:x3="-4.0901443513" cml:y3="-3.10664597908" cml:z3="-1.18904146578"/>
            <cml:atom cml:elementType="C" cml:id="a2" cml:formalCharge="0" cml:x3="-0.564202390594" cml:y3="-2.0116665412" cml:z3="0.507612692817"/>
            <cml:atom cml:elementType="C" cml:id="a3" cml:formalCharge="0" cml:x3="-1.92163762747" cml:y3="-1.98241261328" cml:z3="-0.206572876019"/>
            <cml:atom cml:elementType="C" cml:id="a4" cml:formalCharge="0" cml:x3="-2.92693672032" cml:y3="-2.98225012861" cml:z3="0.367318952425"/>
            <cml:atom cml:elementType="O" cml:id="a5" cml:formalCharge="0" cml:x3="-4.208010389" cml:y3="-2.9017666367" cml:z3="-0.248445606999"/>
            <cml:atom cml:elementType="H" cml:id="a6" cml:formalCharge="0" cml:x3="0.655827908609" cml:y3="-1.26752859656" cml:z3="-1.14312376632"/>
            <cml:atom cml:elementType="H" cml:id="a7" cml:formalCharge="0" cml:x3="0.0848553777946" cml:y3="-0.00239243828223" cml:z3="-0.0456709489122"/>
            <cml:atom cml:elementType="H" cml:id="a8" cml:formalCharge="0" cml:x3="1.40666917559" cml:y3="-1.07214682201" cml:z3="0.448356319391"/>
            <cml:atom cml:elementType="H" cml:id="a9" cml:formalCharge="0" cml:x3="-0.154987987004" cml:y3="-3.03150093117" cml:z3="0.472873914607"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a6" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a7" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a8" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a10" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a3" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a9" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a11" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a12" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a4" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a13" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a14" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a5" cml:order="1"/>
            <cml:bond cml:atomRefs2="a5 a15" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:Surfaces>
            <ssip:Surface>
                <ssip:TotalSurfaceArea ssip:unit="Å^2">122.2095781704891</ssip:TotalSurfaceArea>
                <ssip:NegativeSurfaceArea ssip:unit="Å^2">20.900319182402814</ssip:NegativeSurfaceArea>
                <ssip:PositiveSurfaceArea ssip:unit="Å^2">101.30925898808572</ssip:PositiveSurfaceArea>
                <ssip:ElectronDensityIsosurface ssip:unit="e bohr^-3">0.002</ssip:ElectronDensityIsosurface>
                <ssip:NumberOFMEPSPoints>9580</ssip:NumberOFMEPSPoints>
                <ssip:VdWVolume ssip:unit="Å^3">99.3920085615431</ssip:VdWVolume>
                <ssip:ElectrostaticPotentialMax ssip:unit="kJ mol^-1">213.86532254634</ssip:ElectrostaticPotentialMax>
                <ssip:ElectrostaticPotentialMin ssip:unit="kJ mol^-1">-172.51370353134</ssip:ElectrostaticPotentialMin>
            </ssip:Surface>
        </ssip:Surfaces>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="3.5" ssip:nearestAtomID="a15" cml:x3="-3.8161781356582187" cml:y3="-3.342557618498982" cml:z3="-2.192649106294994"/>
        <ssip:SSIP ssip:value="0.773639545237379" ssip:nearestAtomID="a12" cml:x3="-2.239105576984704" cml:y3="-3.099174713898159" cml:z3="-2.0252301249653697"/>
        <ssip:SSIP ssip:value="0.5071846345423165" ssip:nearestAtomID="a13" cml:x3="-2.8499750929763277" cml:y3="-4.524230524251444" cml:z3="-0.8462517897648159"/>
        <ssip:SSIP ssip:value="0.4784372492163057" ssip:nearestAtomID="a6" cml:x3="0.9270005337214728" cml:y3="-1.649107340870889" cml:z3="-2.251805301783455"/>
        <ssip:SSIP ssip:value="0.46383776537503163" ssip:nearestAtomID="a9" cml:x3="0.136470579099108" cml:y3="-4.205322913496092" cml:z3="0.352187567944962"/>
        <ssip:SSIP ssip:value="0.4557099985213939" ssip:nearestAtomID="a8" cml:x3="2.420223369759191" cml:y3="-1.188501408973558" cml:z3="1.0796321860050409"/>
        <ssip:SSIP ssip:value="0.4355872186992693" ssip:nearestAtomID="a7" cml:x3="-0.30271267022695597" cml:y3="1.1313523827905538" cml:z3="0.031343697635518995"/>
        <ssip:SSIP ssip:value="0.43429343831228867" ssip:nearestAtomID="a14" cml:x3="-2.937811637006091" cml:y3="-2.45801773806002" cml:z3="2.5781764284587028"/>
        <ssip:SSIP ssip:value="0.33435822085360595" ssip:nearestAtomID="a11" cml:x3="-2.5158938537831497" cml:y3="0.207568717565752" cml:z3="0.059691722864448994"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a3" cml:x3="-1.4487933333328935" cml:y3="-2.074966666667215" cml:z3="0.05005333333293337"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a3" cml:x3="-1.4487933333328935" cml:y3="-2.074966666667215" cml:z3="0.05005333333293337"/>
        <ssip:SSIP ssip:value="-5.3386085001748205" ssip:nearestAtomID="a5" cml:x3="-5.4850745889327115" cml:y3="-3.640825199834338" cml:z3="0.48092898414517593"/>
        <ssip:SSIP ssip:value="-6.9" ssip:nearestAtomID="a5" cml:x3="-5.353424285394745" cml:y3="-1.7849561367024218" cml:z3="0.11271581239149799"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.mol_info_list = [
            {
                "concentration_value": 10.8702104695089,
                "inchikey": "LRHPLDYGYMQRHN-UHFFFAOYSA-N",
                "mole_fraction": 0.16452830188679246,
                "ssip_molecule": self.expected_butanol_xml,
            },
            {
                "concentration_value": 55.3496115427303,
                "inchikey": "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                "mole_fraction": 0.8354716981132075,
                "ssip_molecule": self.expected_water_xml,
            },
        ]
        self.mole_fraction_dict_list = [
            {"water": 0.8354716981132075, "1-butanol": 0.16452830188679246}
        ]
        self.solvent_info_list = phasexmlmaker.generate_solv_info_list(
            self.mole_fraction_dict_list
        )
        self.expected_phase_file = (
            (parent_directory / "resources" / "expected_phase.xml")
            .absolute()
            .as_posix()
        )
        self.expected_solvent_file = (
            (parent_directory / "resources" / "expected_solvent.xml")
            .absolute()
            .as_posix()
        )
        self.temperature_info = [
            {"temperature_value": 298.0, "temperature_units": "KELVIN"}
        ]
        self.phase_filename = "actual_phase.xml"
        self.solvent_filename = "actual_solvent.xml"

    def tearDown(self):
        """Clean up after tests.

        Returns
        -------
        None.

        """
        if os.path.isfile(self.phase_filename):
            os.remove(self.phase_filename)
        if os.path.isfile(self.solvent_filename):
            os.remove(self.solvent_filename)

    def test_create_solvent_file(self):
        """Test expected file produced.

        Returns
        -------
        None.

        """
        phasexmlmaker.create_solvent_file(
            self.mole_fraction_dict_list, self.solvent_filename
        )
        with open(self.solvent_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_solvent_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_create_phase_file(self):
        """Test expected file produced.

        Returns
        -------
        None.

        """
        phasexmlmaker.create_phase_file(
            [[self.temperature_info, self.mole_fraction_dict_list]], self.phase_filename
        )
        with open(self.phase_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phase_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_write_solvent_file(self):
        """Test expected file produced.

        Returns
        -------
        None.

        """
        phasexmlmaker.write_solvent_file(self.solvent_info_list, self.solvent_filename)
        with open(self.solvent_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_solvent_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_write_phase_file(self):
        """Test expected file produced.

        Returns
        -------
        None.

        """
        phasexmlmaker.write_phase_file(
            [[self.temperature_info, self.solvent_info_list]], self.phase_filename
        )
        with open(self.phase_filename, "r") as act_file:
            actual_contents = act_file.read()
            with open(self.expected_phase_file, "r") as exp_file:
                expected_contents = exp_file.read()
                self.assertMultiLineEqual(expected_contents, actual_contents)

    def test_generate_solv_info_list(self):
        """Test expected list produced.

        Returns
        -------
        None.

        """
        expected_list = self.mol_info_list
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_info_list = self.solvent_info_list
        self.assertEqual(1, len(actual_info_list))
        actual_information = actual_info_list[0]
        self.assertEqual(
            "1-butanol0.164528302water0.835471698", actual_information["solvent_name"]
        )
        self.assertEqual(
            "1-butanol0.164528302water0.835471698", actual_information["solvent_id"]
        )
        actual_list = actual_information["ssip_info_list"]
        for entry, information_dict in enumerate(actual_list):
            with self.subTest(entry=entry):
                expected_dict = expected_list[entry]
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    expected_dict["inchikey"], information_dict["inchikey"]
                )
                self.assertAlmostEqual(
                    expected_dict["concentration_value"],
                    information_dict["concentration_value"],
                )
                self.assertAlmostEqual(
                    expected_dict["mole_fraction"], information_dict["mole_fraction"]
                )
                self.assertMultiLineEqual(
                    expected_dict["ssip_molecule"],
                    etree.tounicode(information_dict["ssip_molecule"]),
                )
