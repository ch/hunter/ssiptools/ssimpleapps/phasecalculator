# -*- coding: utf-8 -*-
#    phasecalculator calculates FGIPs, solvent similarity and VLE with SSIMPLE.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasecalculator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script with CLI for phasecalculator.

@author: Mark
"""

import logging
import argparse
import textwrap
import puresolventinformation.information as pureinf
import phasecalculator.runners.phasecalculatorrunner as phasecrun
import phasecalculator.io.systemcollectionprocessor as sysproc
import phasecalculator.io.phasecsvconverter as csvconv
from phasecalculator.classes.runtimeinformation import RuntimeInformation
from phasecalculator.classes.outputinformation import OutputInformation
from phasecalculator.classes.system import System, SystemCollection

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

INFO_LOGGER = logging.getLogger(__name__)
INFO_LOGGER.setLevel(logging.INFO)


def main():
    """Run function when program called.

    Returns
    -------
    None
        Process result.

    """
    # Create parser
    parser = create_phasecalculator_argparser()
    LOGGER.info("created porser")
    # parse args
    args = parser.parse_args()
    LOGGER.info("parsed args:")
    LOGGER.info(args)
    return process_args(args)


def process_args(args):
    """Process CLI Arguments.

    Parameters
    ----------
    args : argparse.Namespace
        Arguments from reading command line.

    Returns
    -------
    None
        Process result.

    """
    return args.func(args)


def process_inputgen(args):
    """Process arguments to run input file creation for phase calculation.

    Parameters
    ----------
    args : argparse.Namespace
        Arguments from reading command line.

    Returns
    -------
    None.

    """
    output_filename = args.filename
    system_collection = create_system_collection(args)
    sysproc.write_system_collection_file(system_collection, output_filename)


def process_phasecalculator(args):
    """Process arguments to run phase calculation.

    Parameters
    ----------
    args : argparse.Namespace
        Arguments from reading command line.

    Returns
    -------
    None.

    """
    xml_filename = args.file
    system_collection = read_calculator_xml(xml_filename)
    LOGGER.info("system collection: ")
    LOGGER.info(system_collection)
    if args.memreq is not None:
        run_system_collection(system_collection, memory_req=args.memreq)
    else:
        run_system_collection(system_collection)


def create_system_collection(args):
    """Create SystemCollection from input arguments.

    Parameters
    ----------
    args : argparse.Namespace
        Arguments from argparse read of command line input.

    Returns
    -------
    SystemCollection
        SystemCollection.

    """
    phases = read_phasecsv(args.phases, args.unpolarised_ssips)
    jar_path = args.jar
    scratch_dir = args.scratch
    output_dir = args.out_dir
    run_inf = create_runtime_inf(jar_path, scratch_dir, output_dir)
    fgip_output = args.fgip
    similarity_output = args.sim
    similarity_output_type = args.sim_type if similarity_output else None
    vle_output = args.vle
    out_inf = create_output_inf(
        fgip_output, similarity_output, similarity_output_type, vle_output
    )
    system = System(phases, run_inf, out_inf)
    return SystemCollection([system])


def create_output_inf(
    fgip_output, similarity_output, similarity_output_type, vle_output
):
    """Create OutputInformation.

    Parameters
    ----------
    fgip_output : bool
        FGIP output result.
    similarity_output : bool
        Similarity output result.
    similarity_output_type : str
        Output type, see schema for restriction.
    vle_output : bool
        VLE output result.

    Returns
    -------
    OutputInformation
        Output information for calculation.

    """
    return OutputInformation(
        fgip_output, similarity_output, similarity_output_type, vle_output
    )


def create_runtime_inf(jar_path, scratch_dir, output_dir):
    """Create RuntimeInformation.

    Parameters
    ----------
    jar_path : str
        Jar file location.
    scratch_dir : str
        Scratch directory path.
    output_dir : str
        Output directory path.

    Returns
    -------
    RuntimeInformation
        Runtime information for calculation.

    """
    return RuntimeInformation(jar_path, scratch_dir, output_dir)


def read_calculator_xml(xml_filename):
    """Read SystemCollection XML.

    Parameters
    ----------
    xml_filename : str
        filename for SystemCollection XML.

    Returns
    -------
    SystemCollection
        System collection for calculations to run.

    """
    return sysproc.read_system_collection_file(xml_filename)


def read_phasecsv(csv_filename, unpolarised_ssips):
    """Read CSV file containing phase infomration and create Phases object.

    Parameters
    ----------
    csv_filename : str
        phase information CSV file.
    unpolarised_ssips : boolean
        True if unpolarised SSIP descriptions are to be used.

    Returns
    -------
    Phases
        Phases representation of information.

    """
    return csvconv.convert_csv_file_to_phases(csv_filename, unpolarised_ssips)


def run_system_collection(system_collection, **kwargs):
    """Run all systems in collection.

    Parameters
    ----------
    system_collection : SystemCollection
        system collection.
    memory_req : str, optional
        Memory settings for jar executable. The default is None.

    Returns
    -------
    None.

    """
    for system_info in system_collection.system_list:
        phasecrun.run_all_analysis(system_info, **kwargs)


def get_acceptable_solvent_names():
    """Get Acceptable solvent names.

    Returns
    -------
    Str
        Solvent names, formatted 1 per line.

    """
    solvent_names = pureinf.get_name_inchikey_mapping().keys()
    return "\n".join(solvent_names)


def solvent_names(args):
    """Output solvent names by using a logger.

    Returns
    -------
    None.

    """
    INFO_LOGGER.info("Solvent names:\n%s", get_acceptable_solvent_names())


def create_phasecalculator_argparser():
    """Create Argument parser for Phasecalculator module.

    Returns
    -------
    ArgumentParser

    """
    description = """Phase Calculator provides methods to perform FGIP,
similarity and VLE analysis for solvents."""
    epilog = """For example usage see submodule help"""
    phase_argparser = argparse.ArgumentParser(
        description=description,
        epilog=epilog,
        conflict_handler="resolve",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    subparsers = phase_argparser.add_subparsers(title="commands", dest="command")

    calc_description = """Calculation runner based on input System XML."""
    calc_epilog = textwrap.dedent(
        """Example Usage:

python -m phasecalculator calculate -f systemcollection.xml

Where systemcollection.xml was generated with inpgen. For large phase sets you need to set memreq to match your current hardware limit."""
    )

    calc_argparser = subparsers.add_parser(
        "calculate",
        description=calc_description,
        epilog=calc_epilog,
        help="calculate phase properties.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    calc_argparser.add_argument(
        "-i",
        "-f",
        "--file",
        type=str,
        required=True,
        help="filename for SystemCollectionXML",
    )
    calc_argparser.add_argument(
        "--memreq",
        type=str,
        default=None,
        help="memory specifier for jar call. Defaults to system default.",
    )
    calc_argparser.set_defaults(func=process_phasecalculator)

    inpgen_description = """Input SystemCollection XML generation."""
    inpgen_epilog = textwrap.dedent(
        """\
  Example usage:
    
    python -m phasecalculator inpgen -p $CONDA_PREFIX/lib/python3.7/site-packages/phasecalculator/test/resources/examplephasecomp.csv -f -j PATH_TO_JAR

  Where PATH_TO_JAR is replaced with the SSIP phasetransfer jar file location.
"""
    )

    inpgen_argparser = subparsers.add_parser(
        "inpgen",
        description=inpgen_description,
        epilog=inpgen_epilog,
        help="create system input xml.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    inpgen_argparser.add_argument("-p", "--phases", type=str, help="")
    inpgen_argparser.add_argument(
        "-j", "--jar", type=str, required=True, help="Phasetransfer jar path."
    )
    inpgen_argparser.add_argument(
        "-x", "--scratch", type=str, default="scratch", help="scratch directory path."
    )
    inpgen_argparser.add_argument(
        "-o",
        "--out_dir",
        type=str,
        default="output",
        help="output directory for calculation results",
    )

    inpgen_argparser.add_argument(
        "-f", "--fgip", action="store_true", help="Calculate FGIPs for input solvents"
    )
    inpgen_argparser.add_argument(
        "-s",
        "--sim",
        action="store_true",
        help="Calculate similarity for input solvents",
    )
    inpgen_argparser.add_argument(
        "--sim_type", type=str, default="all", help="similarity output type."
    )
    inpgen_argparser.add_argument(
        "-v", "--vle", action="store_true", help="Calculate VLE for input solvents"
    )
    inpgen_argparser.add_argument(
        "--filename",
        type=str,
        default="systemcollection.xml",
        help="filename for XML file.",
    )
    ssip_desc = inpgen_argparser.add_mutually_exclusive_group(required=False)
    ssip_desc.add_argument(
        "--polarised",
        action="store_false",
        dest="unpolarised_ssips",
        help="Use unpolarised SSIP descriptions in calculations",
    )
    ssip_desc.add_argument(
        "--unpolarised",
        action="store_true",
        dest="unpolarised_ssips",
        help="Use polarised SSIP descriptions in calculations",
    )
    inpgen_argparser.set_defaults(func=process_inputgen, unpolarised_ssips=False)
    solvname_description = """Display acceptable solvent molecule names for inclusion in a phases csv file."""
    solvnames_epilog = (
        "Solvent molecule names:\n" + get_acceptable_solvent_names() + "\n"
    )
    solvent_nameparser = subparsers.add_parser(
        "solventnames",
        description=solvname_description,
        epilog=solvnames_epilog,
        help="Get list of aceptable pure solvent names.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    solvent_nameparser.set_defaults(func=solvent_names)
    return phase_argparser
