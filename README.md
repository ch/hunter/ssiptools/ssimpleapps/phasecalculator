# Phasecalculator #

Phasecalculator is a python package designed to automate calculation of FGIPs, solvent similarity and VLE information using SSIMPLE, pulling together analysis and calculation modules in a higher level repository for easier use by experimental chemists.

## How do I get set up? ##

    
### Clone this repository ###

*From bitbucket:*

        git clone https://bitbucket.org/mdd31/phasecalculator.git


*From University of Cambridge gitlab (using ssh):*

    git clone git@gitlab.developers.cam.ac.uk:ch/hunter/ssiptools/ssimpleapps/phasecalculator.git 

If you have not already set up ssh access to the gitlab server please look through the instructions on the gitlab help page [https://gitlab.developers.cam.ac.uk/help/ssh/README](https://gitlab.developers.cam.ac.uk/help/ssh/README).



### Setting up the python environment ###

Change to the repository:

        cd phasecalculator

Details of an anaconda environment is provided in the repository with the required dependencies for this package.

        conda env create -f environment.yml

Note that if you are on a mac then you need to use this command instead:

        conda env create -f macenvironment.yml

This creates and environment callled 'resultsanalysis' which can be loaded using:

        conda activate resultsanalysis

### Install dependencies ###

Before installing this module please make sure that the required modules listed below are installed in the current python environment (resultsanalysis).

Before cloning the dependent repositories please change up a directory and follow install instructions to install within the readme files for the repository.
Do not follow the instructions to create a python environment.

#### Required modules ####

The other modules produced by Mark Driver required for this work to function are listed below. Please install them in the below order.

* xmlvalidator [bitbucket](https://bitbucket.org/mdd31/xmlvalidator) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/utils/xmlvalidator)
* puresolventinformation [bitbucket](https://bitbucket.org/mdd31/puresolventinformation) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/ssimpleapps/puresolventinformation)
* resultsanalysis [bitbucket](https://bitbucket.org/mdd31/resultsanalysis) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/utils/resultsanalysis)
* phasexmlparser [bitbucket](https://bitbucket.org/mdd31/phasexmlparser) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/utils/phasexmlparser)
* phasexmlcreator [bitbucket](https://bitbucket.org/mdd31/phasexmlcreator) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/ssimpleapps/phasexmlcreator)
* solventmapcreator [bitbucket](https://bitbucket.org/mdd31/solventmapcreator) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/ssimpleapps/solventmapcreator)
* ssip (specifically phasetransfer module) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssip)

For the SSIP phasetransfer module please compile a jar with dependencies as detailed in the repository documentation (This requires version 6.0.0 or greater).

### Using pip to install module ###

In the top level phasecalculator folder (where environment.yml and README.md files are located) using pip run the following command in your chosen python environment:

        pip install .

This installs it in your current python environment.


### Expected usage ###

This module contains methods for the running of FGIP, similarity and VLE calculations for solvent mixtures at specified temperatures.
The calculation process can be run through the use of an XML data format.
For the specification of this format please see the Phase Calculator schema on the [Hunter Group website](http://www-hunter.ch.cam.ac.uk/schema).
The CLI has been implemented to enable operation in two modes:

1) Run calculations from input XML.

This uses the calculate option of the CLI.

    python -m phasecalculator calculate -h
    usage: __main__.py calculate [-h] -i FILE [--memreq MEMREQ]

    Calculation runner based on input System XML.

    optional arguments:
      -h, --help            show this help message and exit
      -i FILE, -f FILE, --file FILE
                            filename for SystemCollectionXML (default: None)
      --memreq MEMREQ       memory specifier for jar call. Defaults to system
                            default. (default: None)

    Example Usage: 
        python -m phasecalculator calculate -f systemcollection.xml

    Where systemcollection.xml was generated with inpgen. For large phase sets you need to set memreq to match your current hardware limit.


2) Generate input XML for default solvents.

This uses the inpgen option of the CLI.

    python -m phasecalculator inpgen -h
    usage: __main__.py inpgen [-h] [-p PHASES] -j JAR [-x SCRATCH] [-o OUT_DIR]
                              [-f] [-s] [--sim_type SIM_TYPE] [-v]
                              [--filename FILENAME]

    Input SystemCollection XML generation.

    optional arguments:
      -h, --help            show this help message and exit
      -p PHASES, --phases PHASES
      -j JAR, --jar JAR     Phasetransfer jar path. (default: None)
      -x SCRATCH, --scratch SCRATCH
                            scratch directory path. (default: scratch)
      -o OUT_DIR, --out_dir OUT_DIR
                            output directory for calculation results (default:
                            output)
      -f, --fgip            Calculate FGIPs for input solvents (default: False)
      -s, --sim             Calculate similarity for input solvents (default:
                            False)
      --sim_type SIM_TYPE   similarity output type. (default: all)
      -v, --vle             Calculate VLE for input solvents (default: False)
      --filename FILENAME   filename for XML file. (default: systemcollection.xml)

    Example usage: 
        python -m phasecalculator inpgen -p $CONDA_PREFIX/lib/python3.7/site-packages/phasecalculator/test/resources/examplephasecomp.csv -f -j PATH_TO_JAR

    Where PATH_TO_JAR is replaced with the SSIP phasetransfer jar file location.


See the help arguments for more information about the options.

#### Example calculations ####

Please see the examples included in the bottom of the help statements for the inpgen and calculate modules.

### Notes about file formats ###

#### CSV file format for phase information ####

The CSV file of phase information used by inpgen to create the phase calculator XML has the following specification.

- The file is tab '\t' delimited, and fields are not quoted.
- One solvent/phase entry per row.
- First column contains the temperature.
- Subsequent columns contain molecule name and molefraction for each molecule present in the solvent/phase.
- Illustrative line is below:

	TEMP	MOL_NAME1	MOL_FRAC1	MOL_NAME2	MOL_FRAC2	...


Default solvent names for inclusion in the CSV file can be found by running:

    python -m phasecalculator solventnames

This lists the accepted names of components for use with the CLI. For more advanced usage please see the source code documentation.

### Documentation ###

Documentation can be rendered using sphinx.

        cd docs
        make html

The rendered HTML can then be found in the build sub folder.

In progress: display of this documentation in a project wiki.

### Contribution guidelines ###

This code has been released under the AGPLv3 license.
If you find any bugs please file an issue ticket.
Submission of pull requests for open issues or improvements are welcomed.

### Who do I talk to? ###

Any queries please contact Mark Driver.

### Licensing ###

This is released under the AGPLv3 license.
