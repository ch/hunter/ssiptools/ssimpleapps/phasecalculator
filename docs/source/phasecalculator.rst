phasecalculator package
=======================

Subpackages
-----------

.. toctree::

   phasecalculator.analysis
   phasecalculator.classes
   phasecalculator.io
   phasecalculator.runners
   phasecalculator.test

Submodules
----------

phasecalculator.phasecalculatorcli module
-----------------------------------------

.. automodule:: phasecalculator.phasecalculatorcli
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: phasecalculator
   :members:
   :undoc-members:
   :show-inheritance:
